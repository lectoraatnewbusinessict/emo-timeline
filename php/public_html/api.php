<?php
require 'myAPI.class.php';
require 'DBService.php';

// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

try {
    $dbService = new DBService();
    $API = new myAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN'], $dbService);
    echo $API->processAPI();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}