<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 10/31/2017
 * Time: 1:47 PM
 */

class QuestionModel implements \JsonSerializable
{
    private $id;
    private $text;
    private $has_other;
    private $is_skippable;
    private $sequence;
    private $answers = array();
    private $step_id;

    /**
     * @return mixed
     */
    public function getStepId()
    {
        return $this->step_id;
    }

    /**
     * @param mixed $step_id
     */
    public function setStepId($step_id)
    {
        $this->step_id = $step_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getHasOther()
    {
        return $this->has_other;
    }

    /**
     * @param mixed $has_other
     */
    public function setHasOther($has_other)
    {
        $this->has_other = self::convertBool($has_other);
    }

    /**
     * @return mixed
     */
    public function getisSkippable()
    {
        return $this->is_skippable;
    }

    /**
     * @param mixed $is_skippable
     */
    public function setIsSkippable($is_skippable)
    {
        $this->is_skippable = self::convertBool($is_skippable);
    }

    /**
     * @return mixed
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param mixed $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return mixed
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param mixed $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    public function addAnswer($answer)
    {
        $this->answers[] = $answer;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }

    /**
     * Set the SQL value of BOOLEAN to true / false for PHP
     */
    private function convertBool($value) {
        if ($value == 0) {
            return false;
        } elseif ($value == 1) {
            return true;
        }
    }
}
