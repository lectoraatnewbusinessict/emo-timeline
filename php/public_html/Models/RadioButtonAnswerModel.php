<?php

/**
 * Created by PhpStorm.
 * User: peter
 * Date: 1-11-17
 * Time: 14:41
 */
class RadioButtonAnswerModel extends AnswerModel
{

   protected $step_id;

    public function __construct()
    {
        $this->setType("radiobutton");
    }

    /**
     * @return mixed
     */
    public function getStepId()
    {
        return $this->step_id;
    }

    /**
     * @param mixed $step_id
     */
    public function setStepId($step_id)
    {
        $this->step_id = $step_id;
    }
}