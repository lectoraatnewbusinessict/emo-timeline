<?php

/**
 * Created by PhpStorm.
 * User: peter
 * Date: 1-11-17
 * Time: 14:44
 */
class SliderAnswerModel extends AnswerModel
{
    protected $min_text;
    protected $max_text;

    public function __construct()
    {
        $this->setType("slider");
    }

    /**
     * @return mixed
     */
    public function getMinText()
    {
        return $this->min_text;
    }

    /**
     * @param mixed $min_text
     */
    public function setMinText($min_text)
    {
        $this->min_text = $min_text;
    }

    /**
     * @return mixed
     */
    public function getMaxText()
    {
        return $this->max_text;
    }

    /**
     * @param mixed $max_text
     */
    public function setMaxText($max_text)
    {
        $this->max_text = $max_text;
    }

}