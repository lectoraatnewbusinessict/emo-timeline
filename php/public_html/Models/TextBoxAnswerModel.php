<?php

/**
 * Created by PhpStorm.
 * User: peter
 * Date: 1-11-17
 * Time: 14:45
 */
class TextBoxAnswerModel extends AnswerModel
{

    protected $min_characters;
    protected $max_characters;


    /**
     * @return mixed
     */
    public function getMinCharacters()
    {
        return $this->min_characters;
    }

    /**
     * @param mixed $min_characters
     */
    public function setMinCharacters($min_characters)
    {
        $this->min_characters = $min_characters;
    }

    /**
     * @return mixed
     */
    public function getMaxCharacters()
    {
        return $this->max_characters;
    }

    /**
     * @param mixed $max_characters
     */
    public function setMaxCharacters($max_characters)
    {
        $this->max_characters = $max_characters;
    }

    /**
     * @return mixed
     */
    public function getPlaceHolder()
    {
        return $this->place_holder;
    }

    /**
     * @param mixed $place_holder
     */
    public function setPlaceHolder($place_holder)
    {
        $this->place_holder = $place_holder;
    }
    private $place_holder;

    public function __construct()
    {
        $this->setType("textbox");
    }
}