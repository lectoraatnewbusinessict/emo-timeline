<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 10/31/2017
 * Time: 1:55 PM
 */
require_once 'Models/AnswerModel.php';
require_once 'Models/QuestionModel.php';
require_once 'Models/RadioButtonAnswerModel.php';
require_once 'Models/SliderAnswerModel.php';
require_once 'Models/StepsModel.php';
require_once 'Models/TextBoxAnswerModel.php';


/**
* Service that helps to access to database concerning the CRUD of questions
*/
class DBService

{
    private function queryDB($query) {
        $link = mysqli_connect("localhost", "root", "root", "emotimeline");

        if(!$link) {
            echo 'Connection error';
        }

        $result = $link->query($query);
        $link->close();

        if ($result) {
            return $result;
        } else {
            echo $link->error;
            return "";
        }

    }

    public function getSteps() {
        $stepsQuery =
            "SELECT *" .
            " FROM steps";
        $stepsResult = $this->queryDB($stepsQuery);
        $stepsArray = array();
        while ($row = mysqli_fetch_assoc($stepsResult)) {
            $stepsArray[] = $row;
        }
        return $stepsArray;
    }

    public function getQuestions($stepId) {
        $questionsQuery =
            "SELECT *" .
            " FROM questions" .
            " WHERE step_id = " .
            "$stepId";

        $questionResult = $this->queryDB($questionsQuery);
        $questions = array();
        while($row = $questionResult->fetch_assoc()){
            $questions[] = $row;
        }
        $questions = self::convert_from_latin1_to_utf8_recursively($questions);
        return $questions;
    }

    public function getAnswers($questionID){
        $answerQuery =
            "SELECT " .
            " qa.checkbox_id, ".
            " qa.radiobutton_id, ".
            " qa.textbox_id, ".
            " qa.slider_id, ".
            " rba.text as rba_text, ".
            " rba.tag as rba_tag,".
            " rba.position as rba_position, ".
            " rba.step_id, ".
            " cba.text as cba_text, ".
            " cba.tag as cba_tag, ".
            " cba.position as cba_position, ".
            " sla.min_text, ".
            " sla.max_text, ".
            " sla.tag as sla_tag, ".
            " sla.position as sla_position, ".
            " tba.tag as tba_tag, ".
            " tba.position as tba_position, ".
            " tba.placeholder, ".
            " tba.min_characters , ".
            " tba.max_characters ".
            " FROM questions_answers as qa".
            " LEFT JOIN radio_button_answers as rba" .
                " ON rba.id = qa.radiobutton_id" .
            " LEFT JOIN checkbox_answers as cba " .
                " ON cba.id = qa.checkbox_id".
            " LEFT JOIN slider_answers as sla " .
                " ON sla.id = qa.slider_id" .
            " LEFT JOIN textbox_answers as tba" .
                " ON tba.id = qa.textbox_id" .
            " WHERE question_id = ".$questionID;

        $answerResult = $this->queryDB($answerQuery);
        $answers = array();
        while($row = $answerResult->fetch_assoc()){
            $answers[] = $row;
        }
        $answers = self::convert_from_latin1_to_utf8_recursively($answers);
        return $answers;
    }

    /**
     * Query to submit new entry to DB
     * @param $entry array to be inserted into DB
     */
    public function insertEntry($entry, $hash) {
        $userId = $this->getUserId($hash);
        $entryQuery =
            "INSERT INTO entries " .
            "(rating, user_id ) " .
            "VALUES (".
            $entry["rating"] . ", " .
            $userId . ")";
        $this->queryDB($entryQuery);
    }

    public function getUserId($hash) {
        if ($hash != "") {
            $userQuery =
                "SELECT *" .
                " FROM users" .
                " WHERE hash='" . $hash . "'";
            $userResult = $this->queryDB($userQuery);
            $res = mysqli_fetch_assoc($userResult);
            return $userId = $res["id"];
        }
    }

    /**
     * Query to insert user answer into DB
     * @param $answer
     */
    public function insertUserAnswer($answer) {
        $latestEntryId = $this->getLatestEntryId();
        $userAnswerQuery =
            "INSERT INTO answers " .
            "(text, question_id, entry_id ) " .
            "VALUES (" .
            "'" . $answer["text"] . "', " .
            $answer["question_id"] . ", " .
            $latestEntryId . ")";
        $this->queryDB($userAnswerQuery);
    }

    /**
     * Get the latest entries id from the DB to use with the answers that are submitted with the entry
     */
    public function getLatestEntryId() {
        $latestEntryIdQuery =
            "SELECT MAX(id) " .
            "FROM entries";
        $result = $this->queryDB($latestEntryIdQuery);
        $id = $result->fetch_assoc();
        return $id["MAX(id)"];
    }

    /**
     * Gets all the answers from the given entry_id
     * @param $entryId
     * @return array with Answers for the given entry
     */
    public function getAnswersForDiaryEntry($entryId) {
        $query =
            "SELECT * " .
            "FROM answers " .
            "WHERE entry_id = " .
            $entryId . ";" ;
        $result = $this->queryDB($query);
        $resultArray = array();
        while ($row = $result->fetch_assoc()) {
            $resultArray[] = $row;
        }
        return $resultArray;
    }

    /**
     * Returns the text of the question
     * @param $questionId id of the question in the db
     * @return mixed
     */
    public function getQuestionText($questionId) {
        $query =
            "SELECT text " .
            "FROM questions " .
            "WHERE id = " .
            $questionId . ";" ;
        $result = $this->queryDB($query);
        $text = $result->fetch_assoc();
        return $text["text"];
    }

    /**
     * Creates a questionmodel based on the answers
     * @param $answers the array of answers
     * @return QuestionModel | null returns the model if all the answers are
     *  of the the same type
     */
    public function getModel($answers){
        $questionModel = new QuestionModel();
        $answerModel = null;
        foreach($answers as $answer){
            if($answerModel !=null && $this->getAnswerType($answer)->getType() != $answerModel->getType() ){
                return "Error";
            } else {
                $answerModel = $this->getAnswerType($answer);
                $questionModel->addAnswer($answerModel);
            }
        }
        if ($questionModel->getAnswers() != null && count($questionModel->getAnswers()) > 0) {
            return $questionModel;
        } else {
            return null;
        }
    }


    private function getAnswerType($answer){

        switch($answer){
            case $answer["checkbox_id"] != null:
                $checkBoxModel = $this->createCheckBoxModel($answer);
                return $checkBoxModel;
            case $answer["slider_id"] != null:
                $sliderModel = $this->createSliderModel($answer);
                return $sliderModel;
            case $answer["textbox_id"] != null;
                $textboxModel = $this->createTextBoxModel($answer);
                return $textboxModel;
            case $answer["radiobutton_id"] != null:
                $radiobuttonModel = $this->createRadioButtonModel($answer);
                return $radiobuttonModel;
            default:
                return null;
        }
    }

    /**
     * Creates a checkbox model from a database answer
     * @param $answer the database answer
     * @return AnswerModel the model
     */
    private function createCheckBoxModel($answer){

        $checkBoxModel = new AnswerModel();
        $checkBoxModel->setId($answer['checkbox_id']);
        $checkBoxModel->setPosition($answer['cba_position']);
        $checkBoxModel->setText($answer['cba_text']);
        $checkBoxModel->setTag($answer['cba_tag']);
        $checkBoxModel->setType('checkbox');
        return $checkBoxModel;
    }

    /**
     * Creates a slidermodel from a database answer
     * @param $answer the database answer
     * @return SliderAnswerModel the model
     */
    private function createSliderModel($answer){
        $sliderModel = new SliderAnswerModel();
        $sliderModel->setId($answer['slider_id']);
        $sliderModel->setPosition($answer['sla_position']);
        $sliderModel->setMinText($answer['min_text']);
        $sliderModel->setMaxText($answer['max_text']);
        $sliderModel->setTag($answer['sla_tag']);
        return $sliderModel;
    }

    /**
     * Creates a textbox model from a database answer
     * @param $answer the database answer
     * @return TextBoxAnswerModel the model
     */
    private function createTextBoxModel($answer){
        $textboxModel = new TextBoxAnswerModel();
        $textboxModel->setId($answer['textbox_id']);
        $textboxModel->setPosition($answer['tba_position']);
        $textboxModel->setPlaceHolder($answer['placeholder']);
        $textboxModel->setMinCharacters($answer['min_characters']);
        $textboxModel->setMaxCharacters($answer['max_characters']);
        $textboxModel->setTag($answer['tba_tag']);
        return $textboxModel;
    }

    /**
     * Creates a radiomodel from a database answer
     * @param $answer the database answer
     * @return RadioButtonAnswerModel the model
     */
    private function createRadioButtonModel($answer){
        $radiobuttonModel = new RadioButtonAnswerModel();
        $radiobuttonModel->setId($answer['radiobutton_id']);
        $radiobuttonModel->setPosition($answer['rba_position']);
        $radiobuttonModel->setText($answer['rba_text']);
        $radiobuttonModel->setTag($answer['rba_tag']);
        $radiobuttonModel->setStepId($answer['step_id']);
        return $radiobuttonModel;
    }

    /**
     * Encode array from latin1 to utf8 recursively
     * @param $dat
     * @return array|string
     */
    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = self::convert_from_latin1_to_utf8_recursively($d);
        return $ret;
    }
}
