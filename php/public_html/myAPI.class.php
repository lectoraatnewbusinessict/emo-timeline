<?php
require_once 'API.class.php';
require_once 'Models/AnswerModel.php';
require_once 'Models/QuestionModel.php';
require_once 'Models/StepsModel.php';
require_once 'Models/QuestionAnswerModel.php';

/**
 * Class MyAPI
 * In this class, the GET/POST/PUT/DELETE calls are initialized for the Emo Timeline / Emo Analyser
 */

class MyAPI extends API
{
    protected $User;
    protected $DBService;


    /**
    * Constructor which creates the $request and adds a questionService
    */
    public function __construct($request, $origin, $service)
    {
        parent::__construct($request);
        $this->DBService = $service;
    }

    protected function register() {
        if ($this->method == 'GET') {
            $data = new DateTime();
            $data_formatted = date_format($data, 'Y-m-d H-m-s');
            $hash = hash('sha512',$data_formatted);

//            $query = "INSERT INTO users (hash) VALUES('".$hash."')";
            $query = "INSERT INTO" . " users" . " (hash)" . " VALUES('" . "$hash')";

            if ($this->queryDB($query)) {
                $query = "SELECT id FROM users WHERE hash='" . $hash . "'";
                $result = $this->queryDB($query);
                $res = mysqli_fetch_assoc($result);


                if ($res === "") {
                    echo "Res is leeg";
                    return array("result"=>false, "message"=>"Something went wrong...");
                } else {
                    $hash = $this->generateHash($res['id']);
                    $query = "UPDATE users SET hash='" . $hash . "' WHERE id=" . $res['id'];

                    if ($this->queryDB($query)) {
                        return array("result"=>true, "key"=>$hash, "id"=>$res['id'], "message"=>"Welkom bij de app, hier kun je beginnen met het posten van berichten.");
                    } else {
                        echo "DB Query failed";
                        return array("result"=>false, "message"=>"Something went wrong...");
                    }

                }

            }
            echo "First query failed";
            return array("result"=>false, "message"=>"Something went wrong...");
        } else {
            return array("result"=>false, "message"=>"Only accepts GET requests");
        }

    }

    protected function sendMessage() {
        if ($this->method == 'POST') {
            $messages = json_decode(file_get_contents('php://input'), true);

            if (sizeof($messages['messages']) > 0) {
                return $this->saveMessages($messages);
            } else {
                return $response['result'] = false;
            }

        } else {
            return array("result"=>false, "message"=>"Only accepts POST requests");
        }
    }

    private function saveMessages($messages) {
        $query = "INSERT INTO messages (message_id, emotion, datum, message, hash, coach_behaviour, coach_message) VALUES";
        $first = true;
        $response = array();

        foreach ($messages['messages'] as $message) {
            $emotion = $this->getEmotion($message['bericht']);
            $coach_response['response'] = $this->getCoachMessage($message);
            if ($first) {
                $query .= " (" . $message['message_id'] . ", '" . $emotion . "', " . $message['datum'] . ", '" . $message['bericht'] . "', '" . $messages['hash'] . "', '" . $message['coach_behaviour'] . "', '". $coach_response['response']  ."')";
                $first = false;
            } else {
                $query .= ", (" . $message['message_id'] . ", '" . $emotion . "', " . $message['datum'] . ", '" . $message['bericht'] . "', '" . $messages['hash'] . "', '" . $message['coach_behaviour'] . "', '". $coach_response['response']  ."')";
            }

            $long_message['id'] = $message['message_id'];
            $long_message['emotions'] = array($emotion);
            $long_message['response'] = $coach_response['response'];

            $response['message_results'][]= $long_message;
        }


        $response['result'] = ($this->queryDB($query) == true);
        return $response;
    }

    protected function prepareTransfer() {
        if ($this->method == 'POST') {
            $data = json_decode(file_get_contents('php://input'), true);
            $query = "SELECT id FROM users WHERE hash='" . $data['hash'] . "'";

            if ($this->queryDB($query)) {
                $query = "SELECT tempKey FROM transfers WHERE tempKey = '" . $data['tempKey'] ."'";

                if(!mysqli_fetch_assoc($this->queryDB($query))['tempKey']) {
                    $query = "INSERT INTO transfers (tempkey, hash, expires) VALUES";
                    $query .= " ('" . $data['tempKey'] . "', '" . $data['hash'] . "', " . (time() + 4200) . ")";

                    if ($this->queryDB($query)) {
                        return array("result"=>true, "message"=>"ready to transfer.");
                    } else {
                        return array("result"=>false, "message"=>"Something went wrong...");
                    }

                } else {
                    return array("result"=>false, "message"=>"Something went wrong...");
                }

            } else {
                return array("result"=>false, "message"=>"Something went wrong...");
            }

        } else {
            return array("result"=>false, "message"=>"Something went wrong...");
        }

    }

    protected function transferData() {
        if ($this->method == 'POST') {
            $data = json_decode(file_get_contents('php://input'), true);
            $query = "SELECT hash FROM transfers WHERE tempKey='" . $data['tempKey'] . "' AND expires > " . time();
            $hash = mysqli_fetch_assoc($this->queryDB($query))['hash'];

            if ($hash) {
                $query = "SELECT * FROM messages WHERE hash='" . $hash . "'";
                $result = $this->queryDB($query);
                $data = array();

                while($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }

                $query = "SELECT id FROM users WHERE hash = '" . $hash . "'";
                $result = $this->queryDB($query);

                if($result) {
                    $row = mysqli_fetch_assoc($result);
                    $id = $row['id'];
                } else {
                    return array("result"=>false, "message"=>"Something went wrong...");
                }

                return array('result'=> true, 'messages'=>$data, 'id'=>$id, 'hash'=>$hash);
            } else {
                return array("result"=>false, "message"=>"Something went wrong...");
            }

        } else {
            return array("result"=>false, "message"=>"Only accepts POST requests");
        }

    }

    protected function getCoachMessage($message) {
        $coach_message = "Bericht ontvangen";
        return $coach_message;
    }

    protected function getEmotion($message = "") {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "message=".$message);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_URL, "http://vc-1.hilab.cmi.hanze.nl/emotagger/tag");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    protected function getDiaryEntries() {
        if ($this->method == 'GET') {
            $hash = "";
            foreach (getallheaders() as $name => $value) {
                if ($name == "hash") {
                    $hash = $value;
                    break;
                }
            }
            if ($hash != ""){
                $userId = $this->DBService->getUserId($hash);

                $entryQuery =
                    "SELECT *" .
                    " FROM entries" .
                    " WHERE user_id='" . $userId . "'" .
                    " ORDER BY created_at DESC;";
                $entryResult = $this->queryDB($entryQuery);
                $entries = array();
                while ($row = mysqli_fetch_assoc($entryResult)) {
                    $entries[] = $row;
                }
                $this->setStatus(200);
                return $entries;
            } else {
                $this->setStatus(403);
            }
        }
    }

    protected function getQuestions() {
        if ($this->method == "GET") {
            $steps = $this->DBService->getSteps();
            $steps = self::convert_from_latin1_to_utf8_recursively($steps);
            $result = array();
            foreach ($steps as $step) {
                $questions = $this->DBService->getQuestions($step["id"]);
                $stepModel = new StepsModel();
                $stepModel->setId($step['id']);
                $stepModel->setTitle($step['title']);
                $stepModel->setNextStep($step['next_step']);
                $stepModel->setTag($step['tag']);
                foreach ($questions as $question) {
                    $answers = $this->DBService->getAnswers($question["id"]);
                    $model = $this->DBService->getModel($answers);
                    if ($model != null) {
                        $model->setId($question["id"]);
                        $model->setText($question["text"]);
                        $model->setHasOther($question["has_other"]);
                        $model->setIsSkippable($question["is_skippable"]);
                        $model->setSequence($question["question_sequence"]);
                        $model->setStepId($question["step_id"]);
                        $stepModel->addQuestion($model);
                    }
                }
               $result[] = $stepModel;
            }
            return $result;
        } else {
            $this->setStatus(400);
        }
    }


    /**
     * POST Request
     * Posts a diary entry to the DB
     */
    protected function entries() {
        if ($this->method == "POST") {
            $hash = "";
            foreach (getallheaders() as $name => $value) {
                if ($name == "hash") {
                    $hash = $value;
                    break;
                }
            }
            if (hash != null) {
                $data = file_get_contents('php://input');
                $entries = json_decode($data, true);
                $this->DBService->insertEntry($entries, $hash);
                $answers = $entries["answers"];
                foreach ($answers as $answer) {
                    $this->DBService->insertUserAnswer($answer);
                }
            } else {
                $this->setStatus(401);
            }
        } else {
            $this->setStatus(400);
        }
    }

    /**
     * GET Request
     * Gets the Diary Entry for the given entry_id
     *
     */
    protected function getDiaryEntryAnswers()
    {
        if ($this->method == "GET") {
            $hash = "";
            $entryId = "";
            foreach (getallheaders() as $name => $value) {
                if ($name == "hash") {
                    $hash = $value;
                }
                if ($name == "entry_id") {
                    $entryId = $value;
                }
            }
            if ($hash != null && $entryId != null) {

                $answers = $this->DBService->getAnswersForDiaryEntry($entryId);
                $modelArray = array();
                foreach ($answers as $answer) {
                    $model = new QuestionAnswerModel();
                    $model->setAnswer($answer["text"]);
                    $questionText = $this->DBService->getQuestionText($answer["question_id"]);
                    $model->setQuestion($questionText);
                    $modelArray[] = $model;
                }
                if (count($modelArray) > 0 && $modelArray != null) {
                    $this->setStatus(200);
                    return $modelArray;

                } else {
                    $this->setStatus(204);
                }

            } else if ($hash == null) {
                $this->setStatus(401);
            } else if ($entryId == null) {
                $this->setStatus(406);
            }
        }
    }

    /**
    * Generates a hash
    */
    private function generateHash($data) {
        $salt = "kgHuus2*()(-@!=TNKnweakbaebkuahrkaht768087+++uawhtahrnouvrhenhtigs&&7237699";
        return hash('sha512', $salt.$data);
    }

    private function queryDB($query) {
        $link = mysqli_connect("localhost", "root", "root", "emotimeline");

        if(!$link) {
            echo 'Connection error';
        }

        $result = $link->query($query);
        $link->close();

        if ($result) {
            return $result;
        } else {
            echo $link->error;
            return "";
        }

    }

    /**
     * Encode array from latin1 to utf8 recursively
     * @param $dat
     * @return array|string
     */
    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = self::convert_from_latin1_to_utf8_recursively($d);
        return $ret;
    }
}
