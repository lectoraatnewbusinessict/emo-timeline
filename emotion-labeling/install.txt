sudo apt-get install python-dev
sudo apt-get install python3-pip
sudo apt-get install libapache2-mod-wsgi-py3

sudo pip3 install virtualenv

mkdir /srv/emotimeline

Create virtual environment and install dependencies:
   cd /srv/emotimeline
   virtualenv venv
   . venv/bin/activate
   sudo pip3 install Flask
   sudo pip3 install -U nltk
   sudo python -m nltk.downloader -d /usr/local/share/nltk_data all
   sudo pip3 install pandas (takes quite a while compling C code)
   deactivate

Create user for process:
   useradd -M flask
   usermod -s /bin/false flask
   usermod -L flask
   adduser flask www-data
   chown -R flask:www-data /srv/emotimeline

Move emotimeline.wsgi to /srv/emotimeline
Move emotagger.conf to /etc/apache2/sites-available
Move other folder contents into /srv/emotimeline/venv

a2ensite emotagger
service apache2 reload