import logging, sys
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/srv/emotimeline/venv"

activate_this = '/srv/emotimeline/venv/bin/activate_this.py'
with open(activate_this) as file:
	exec(file_.read(), dict(__file__=activate_this))
from webExperiment import app as application