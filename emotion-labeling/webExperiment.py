from flask import Flask, request, session, g, redirect, url_for, abort, render_template;
import pandas as pd
from tagEmotions import getEmotionTagsForText, EmotionTag

lexicon = pd.read_csv('/srv/emotimeline/venv/Emotion-Lexicon.csv', sep=';', encoding='utf-8') # , index_col='Dutch'
print(lexicon)

app = Flask(__name__)


@app.route("/")
def testEmotionTagging():
    return ""


@app.route("/tagEmotions", methods=['POST'])
def tagEmotionsForInput():
    text=request.form['message']
    tags=getEmotionTagsForText(lexicon, text)
	emotion = ""
	freq = 0
	for key, tag in tags.items():
		if tag.frequency > freq and key != 'Positief' and key != 'Negatief'
			emotion = key
			freq = tag.frequency
	return emotion

# TODO: add a simple way to change/update the lexicon? (upload/download ??)

if __name__ == "__main__":
    app.run()
