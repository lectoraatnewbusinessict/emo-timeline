import pandas as pd
import nltk

LANGUAGE = 'dutch'
LEXICON_WORD_COLUMN = 'Dutch'

class EmotionTag:
    def __init__(self, name):
        self.name=name
        self.frequency = 0
        self.factor = 0 # currently not used
        self.tokens=[]

    def print(self):
        print(self.name)
        print("frequency:", self.frequency)
        print("tokens:", id(self.tokens), self.tokens)

def createTagsDictionaryForLexicon(lexicon):
    tags = {}
    for emotion in list(lexicon.columns)[2:]: #first two columns contain the words, so skip them
        tags[emotion] = EmotionTag(emotion)
    return tags

def getEmotionTagsForText(lexicon, text):
    # use tweet tokenizer (?)
    # use sentence tokenizer to support multiple sentences (?)
    tokens = nltk.word_tokenize(text, language = LANGUAGE)
    emotionTags=createTagsDictionaryForLexicon(lexicon)
    for token in tokens:
        resultRowsByWord = lexicon[lexicon[LEXICON_WORD_COLUMN] == token]
        for emotionName in emotionTags.keys():
            resultCol=resultRowsByWord[emotionName]
            if resultCol.values.size>0 and resultCol.values[0]:
                emotionTags[emotionName].frequency+=1
                emotionTags[emotionName].tokens.append(token)
    return emotionTags

#lexicon = pd.read_csv('Emotion-Lexicon.csv', sep=';', encoding='utf-8') # , index_col='Dutch'
#sentence = "Ik ben erg boos en angstig, boos, boos."
#for tag in getEmotionTagsForText(lexicon, sentence).values():
#    tag.print()

