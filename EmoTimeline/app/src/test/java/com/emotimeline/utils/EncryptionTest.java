package com.emotimeline.utils;

import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

/**
 * Tests the encryption class for the app.
 *
 * @author Kevin
 */

public class EncryptionTest {

    @Test
    public void sha256Test() {
        assertNotEquals(Encryption.sha256("testString"), "testString");
    }
}
