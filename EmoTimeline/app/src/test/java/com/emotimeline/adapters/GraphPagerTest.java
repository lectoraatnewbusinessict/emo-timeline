package com.emotimeline.adapters;

import android.support.v4.app.FragmentManager;
import android.test.mock.MockContext;
import com.emotimeline.fragments.LineGraphFragment;
import com.emotimeline.fragments.PieGraphFragment;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;


public class GraphPagerTest {

    @Mock
    FragmentManager fm;

    @Mock
    MockContext context;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void graphPager_getCount_returnsTwo() {
        GraphPagerAdapter adapter = new GraphPagerAdapter(fm);
        assertEquals(adapter.getCount(), 2);
    }

    @Test
    public void graphPager_getItem_returnsRightResult(){
        GraphPagerAdapter adapter = new GraphPagerAdapter(fm);
        assertThat(adapter.getItem(0), instanceOf(PieGraphFragment.class));
        assertThat(adapter.getItem(1), instanceOf(LineGraphFragment.class));
    }


}
