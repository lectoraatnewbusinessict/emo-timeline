package com.emotimeline.adapters;

import android.util.Log;
import com.emotimeline.models.localdb.Message;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MessageAdapterTest {
    private ArrayList<Message> messages;

    @Before
    public void setUp(){
        messages = new ArrayList<>();
        String sDate = "23-04-2016 15:10:45";
        String sDate2 = "23-04-2016 15:10:555";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.forLanguageTag("nl_NL"));
        try {
            long date  = sdf.parse(sDate).getTime();
            long date2 = sdf.parse(sDate2).getTime();

            Message messageCoach = new Message();
            Message messageUser = new Message();
            messageCoach.setSender(Message.Sender.COACH);
            messageCoach.setDate(date);
            messageCoach.setMessage("Ik ben de coach");
            messageUser.setSender(Message.Sender.USER);
            messageUser.setMessage("Ik ben een gebruiker");
            messageUser.setDate(date2);
            messages.add(messageCoach);
            messages.add(messageUser);
            messages.add(messageCoach);

        } catch(ParseException e) {
            Log.e("MessageParserTest", "cant parse date string in test");
        }

    }

    @Test
    public void MessageAdapter_getViewType_ReturnsType() {
        MessageAdapter adapter = new MessageAdapter(messages);
        assertEquals(adapter.getItemViewType(0), 100) ;
        assertEquals(adapter.getItemViewType(1), Message.Sender.USER.ordinal());
        assertEquals(adapter.getItemViewType(2), Message.Sender.COACH.ordinal());
    }

    @Test
    public void MessageAdatper_getItemCount_ReturnsTwo(){
        MessageAdapter adapter = new MessageAdapter(messages);
        assertEquals(adapter.getItemCount(), 2);
    }


}
