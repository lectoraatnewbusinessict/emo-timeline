package com.emotimeline.modeltests;

import com.emotimeline.models.DiaryEntryModel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by peter on 18-10-17.
 */

public class DiaryModelTest {

    @Test
    public void getDate_Returns_Proper_String(){
        DiaryEntryModel model = new DiaryEntryModel();
        model.setEntryDate(1508331051);
        assertEquals(model.getFormattedEntryDate(), "18 October 2017 14:50");
    }
}


