package com.emotimeline;

import android.content.Context;
import android.content.Intent;

import com.emotimeline.activities.IntroActivity;

import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by Frank on 10/11/2017.
 */

public class IntroActivityTest {

    @Mock
    Context context;

    @Test
    public void introActivityNotNull()
    {
        Intent intent = new Intent(context, IntroActivity.class);
        assertNotNull("The IntroActivity should not be empty", intent );
    }
}
