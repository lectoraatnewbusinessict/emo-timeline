package com.emotimeline;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.emotimeline.activities.MainActivity;
import com.emotimeline.models.localdb.User;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;


/**
 * Created by Frank on 10/16/2017.
 */
@RunWith(AndroidJUnit4.class)
public class MainActTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivity_isFirstLogin(){
        MainActivity mainActivity = mActivityRule.getActivity();
        assertFalse(mainActivity.firstStart);
    }

    @Test
    public void mainActivity_setCoachData_getName(){
        MainActivity mainActivity = mActivityRule.getActivity();
        mainActivity.setCoachData();
        User user =  mainActivity.getUser();
        user.setCoachName("TestName");
        assertEquals("TestName", user.getCoachName());
    }
}
