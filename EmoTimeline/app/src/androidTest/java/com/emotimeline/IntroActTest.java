package com.emotimeline;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import com.emotimeline.activities.IntroActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * This Test is designed to test if the number of slides of the Intro Activity on creation is
 * equal to six, and thus, created correctly.
 * @author Frank
 */

@RunWith(AndroidJUnit4.class)
public class IntroActTest {
    @Rule
    public ActivityTestRule<IntroActivity> mActivityRule =
            new ActivityTestRule<>(IntroActivity.class);

    @Test
    public void introActivity_getSlides_returnSix()
    {
        IntroActivity act = mActivityRule.getActivity();
        List<android.support.v4.app.Fragment> slides = act.getSlides();
        assertEquals(6, slides.size());
    }

}
