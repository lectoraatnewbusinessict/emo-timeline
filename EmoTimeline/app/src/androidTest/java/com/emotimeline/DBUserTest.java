package com.emotimeline;

/**
 * Created by Koen on 18-10-2016.
 */

import com.emotimeline.models.localdb.User;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.localdb.DatabaseContract;
import com.emotimeline.localdb.DatabaseHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.InstrumentationRegistry.getTargetContext;
@RunWith(JUnit4.class)
public class DBUserTest {

    private DatabaseHelper db;

    @Before
    public void setUp() {
        getTargetContext().deleteDatabase(DatabaseContract.Users.TABLE_NAME);
        db = new DatabaseHelper(getTargetContext());
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void shouldSaveUser() {
        DBUser dbUser = new DBUser(getTargetContext());
        assert(insertNewUser(dbUser) != -1);
    }

    @Test
    public void shouldGetUser() {
        DBUser dbUser = new DBUser(getTargetContext());
        insertNewUser(dbUser);
        assert(dbUser.getUser() != null);
    }

    private long insertNewUser(DBUser dbUser) {
        User user = new User();
        user.set_id(1);
        user.setKey("KEY");
        return dbUser.save(user);
    }
}
