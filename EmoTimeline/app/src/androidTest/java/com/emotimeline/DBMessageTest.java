package com.emotimeline;


import com.emotimeline.models.localdb.Message;
import com.emotimeline.localdb.DBMessage;
import com.emotimeline.localdb.DatabaseContract;
import com.emotimeline.localdb.DatabaseHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.InstrumentationRegistry.getTargetContext;


/**
 * Created by Koen on 18-10-2016.
 */
@RunWith(JUnit4.class)
public class DBMessageTest {

    private DatabaseHelper db;

    @Before
    public void setUp() {
        getTargetContext().deleteDatabase(DatabaseContract.Message.TABLE_NAME);
        db = new DatabaseHelper(getTargetContext());
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Test
    public void shouldSaveMessage() {
        DBMessage dbMessage = new DBMessage(getTargetContext());
        assert(insertNewMessage(dbMessage) != -1);
    }

    @Test
    public void shouldGetMessages() {
        DBMessage dbMessage = new DBMessage(getTargetContext());
        insertNewMessage(dbMessage);
        assert(dbMessage.getMessages().size() > 0);
    }

    private long insertNewMessage(DBMessage dbMessage) {
        Message message = new Message();
        message.setMessage("Message");
        message.setSender(Message.Sender.USER);
        return dbMessage.save(message);
    }

}
