package com.emotimeline.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.emotimeline.R;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * This service provides all functionality for the local notifications.
 *
 * @author Kevin
 */
public class NotificationService {

    private static final int DAILY_REMINDER_REQUEST_CODE = 4582;

    /**
     * Set a daily reminder for a given hour and minute.
     *
     * @param context the context that sets the reminder
     * @param cls the class that receives the notification
     * @param hour an Integer representing the hour on a 24 hour clock
     * @param minute an Integer representing the minute in an hour
     */
    public static void setReminder(Context context, Class<?> cls, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        Calendar timedCalendar = getCalendarForTime(hour, minute);
        cancelReminder(context, cls);

        if(timedCalendar.before(calendar))
            timedCalendar.add(Calendar.DATE,1);

        ComponentName receiver = new ComponentName(context, cls);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        setupAlarmManager(context, cls, timedCalendar);
    }

    /**
     * When a alarm is received this method creates and shows the notification.
     *
     * @param context the Context that creates the notification
     * @param cls the Class that receives the notification
     * @param title the title to be shown in the notification
     * @param content the content to be shown in the notification
     */
    public static void showNotification(Context context, Class<?> cls, String title, String content) {
        Intent notificationIntent = new Intent(context, cls);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(cls);
        stackBuilder.addNextIntent(notificationIntent);

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(DAILY_REMINDER_REQUEST_CODE,
                    getBuiltNotification(stackBuilder, context, title, content));
        }
    }

    /**
     * Cancels the alarms that were already scheduled.
     *
     * @param context the context that creates the alarm
     * @param cls the class that receives the alarm
     */
    private static void cancelReminder(Context context, Class<?> cls) {
        ComponentName receiver = new ComponentName(context, cls);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent = new Intent(context, cls);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                DAILY_REMINDER_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        if (am != null) {
            am.cancel(pendingIntent);
        }
        pendingIntent.cancel();
    }

    /**
     * Initialize the AlarmManager for a daily alarm at a given time.
     *
     * @param context the context that creates the reminder
     * @param cls the Class that receives the notification
     * @param cal the Calendar set at the time the notification needs to be shown
     */
    private static void setupAlarmManager(Context context, Class<?> cls, Calendar cal) {
        Intent intent = new Intent(context, cls);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                DAILY_REMINDER_REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        if (am != null) {
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    /**
     * Create a Calendar for an hour and a minute.
     *
     * @param hour an Integer representing the hour on a 24 hour clock
     * @param minute an Integer representing the minute in an hour
     * @return the created Calendar
     */
    private static Calendar getCalendarForTime(int hour, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }

    /**
     * Build and return a Notification from the given parameters.
     *
     * @param stackBuilder used to get the PendingIntent from the stack
     * @param context the Context that creates the notification
     * @param title the title to be shown in the notification
     * @param content the content to be shown in the notification
     * @return the Notification that needs to be shown
     */
    private static Notification getBuiltNotification(TaskStackBuilder stackBuilder, Context context,
                                          String title, String content) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(
                DAILY_REMINDER_REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        return builder.setContentTitle(title)
                .setContentText(content).setAutoCancel(true)
                .setSound(alarmSound)
                .setSmallIcon(R.mipmap.ic_launcher_notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round))
                .setColor(context.getResources().getColor(R.color.color_primary))
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent).build();
    }
}
