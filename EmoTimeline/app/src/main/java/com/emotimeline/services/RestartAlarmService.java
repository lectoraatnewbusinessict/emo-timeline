package com.emotimeline.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.User;
import com.emotimeline.receivers.AlarmReceiver;

/**
 * This represents a service which is used to restart the alarms used for the NotificationService.
 *
 * @author Kevin
 */
public class RestartAlarmService extends IntentService {

    public RestartAlarmService() {
        super("RestartAlarmService");
    }

    /**
     * This method resets the notification times for the AlarmManager.
     *
     * @param intent the intent that calls the method
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        User user = new DBUser(this).getUser();
        NotificationService.setReminder(this, AlarmReceiver.class,
                user.getNotificationHour(), user.getNotificationMinute());
    }
}
