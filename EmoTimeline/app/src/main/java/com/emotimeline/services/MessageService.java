package com.emotimeline.services;

import android.content.Context;

import com.emotimeline.models.localdb.Message;
import com.emotimeline.fragments.TimelineFragment;
import com.emotimeline.localdb.DBMessage;
import com.emotimeline.models.response.MessageResponse;
import com.emotimeline.models.response.MessageResult;

/**
 * Creates a service which can be used to retrieve and send messages.
 *
 * @author Kevin
 */
public abstract class MessageService {

    /**
     * Sync messages from coach and user.
     *
     * @param context context in which the method is called
     * @param messageResponse response model from API
     */
    public static void syncMessages(Context context, MessageResponse messageResponse) {
        for (MessageResult messageResult : messageResponse.getMessageResults()) {
            saveCoachMessage(context, messageResult.getResponse());
            updateSendMessage(context, messageResult);
        }
    }

    /**
     * Locally save the response from the coach and updates the timeline.
     *
     * @param context Context in which the message is received
     * @param coachResponse the response from the coach
     */
    private static void saveCoachMessage(Context context, String coachResponse) {
        if( coachResponse != null && !coachResponse.isEmpty() ) {
            Message coachMessage = new Message(coachResponse, Message.Sender.COACH);
            new DBMessage(context).save(coachMessage);
            TimelineFragment.messages.add(coachMessage);
            TimelineFragment.messageAdapter.notifyItemInserted(TimelineFragment.messages.size()-1);
        }
    }

    /**
     * When the send message is received by the API, update the message locally.
     *
     * @param context Context in which the message is received
     * @param messageResult the message that need to be updated
     */
    private static void updateSendMessage(Context context, MessageResult messageResult){
        Integer id = messageResult.getId();

        String emotion = messageResult.getEmotions() != null &&
                messageResult.getEmotions().size() > 0 ?messageResult.getEmotions().get(0):null;

        Message messageDB = new DBMessage(context).getMessageById(id);
        messageDB.setSynced(true);
        messageDB.setEmotion(emotion);
        new DBMessage(context).update(messageDB);

        Integer position = 0;
        for( Message message : TimelineFragment.messages ) {
            if( message.get_id().equals(id)) {
                message.setSynced(true);
                break;
            }
            position++;
        }

        TimelineFragment.messageAdapter.notifyItemChanged(position);
    }
}
