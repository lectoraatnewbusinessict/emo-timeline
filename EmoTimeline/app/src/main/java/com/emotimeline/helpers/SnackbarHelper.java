package com.emotimeline.helpers;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Used to easily create a Snackbar from everywhere in the app. This class provides informational
 * and error Snackbars.
 *
 * @author Kevin
 */
public class SnackbarHelper {

    /**
     * Show error Snackbar with given error text and button text. When the button is clicked use the
     * OnClickListener to perform an action.
     *
     * @param view the View to find the parent from
     * @param errorText the error text resource
     * @param buttonText the button text resource
     * @param onClickListener the action for the button
     */
    public static void showErrorSnackbar(View view, int errorText, int buttonText,
                                         @Nullable View.OnClickListener onClickListener) {
        Snackbar snackbar = Snackbar.make(view, errorText, Snackbar.LENGTH_LONG);

        if (buttonText != 0 && onClickListener != null) {
            snackbar.setAction(buttonText, onClickListener);
            snackbar.setActionTextColor(Color.RED);
        }
        TextView textView = snackbar.getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    /**
     * Show informational Snackbar with given text.
     *
     * @param view the View to find the parent from
     * @param infoText the informational text resource
     */
    public static void showInfoSnackbar(View view, int infoText) {
        Snackbar.make(view, infoText, Snackbar.LENGTH_LONG).show();
    }
}
