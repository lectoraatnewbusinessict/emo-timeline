package com.emotimeline.helpers;

import com.emotimeline.R;

/**
 * Methods for the emoticons.
 *
 * @author Kevin
 */
public class EmojiHelper {

    /**
     * Gets the proper icon according to the rating.
     * @param rating the rating set in the database
     * @return the icon located locally
     */
    public static int getEmoticon(int rating){
        switch(rating){
            case 1:
                return R.drawable.emoji_1_transparent;
            case 2:
                return R.drawable.emoji_2_transparent;
            case 3:
                return R.drawable.emoji_3_transparent;
            case 4:
                return R.drawable.emoji_4_transparent;
            case 5:
                return R.drawable.emoji_5_transparent;
            default:
                return R.drawable.ic_do_not_disturb_alt_grey_24dp;
        }
    }

    /**
     * Gets the proper icon according to the rating.
     * @param rating the rating set in the database
     * @return the icon located locally
     */
    public static int getWhiteEmoticon(int rating){
        switch(rating){
            case 1:
                return R.drawable.emoji_1_white;
            case 2:
                return R.drawable.emoji_2_white;
            case 3:
                return R.drawable.emoji_3_white;
            case 4:
                return R.drawable.emoji_4_white;
            case 5:
                return R.drawable.emoji_5_white;
            default:
                return R.drawable.ic_do_not_disturb_alt_grey_24dp;
        }
    }
}
