package com.emotimeline;

import android.app.Application;
import android.support.annotation.Nullable;

import com.emotimeline.models.AnswerModel;
import com.emotimeline.models.DiaryEntryModel;
import com.emotimeline.models.GivenAnswerModel;
import com.emotimeline.models.QuestionModel;
import com.emotimeline.models.StepModel;

import java.util.ArrayList;
import java.util.List;

/**
 * The Application available in the entire app. This Application holds the information needed for a
 * new diary entry.
 *
 * @author Kevin
 */
public class AppController extends Application {

    private static AppController mInstance;
    private List<StepModel> steps;
    private List<GivenAnswerModel> givenAnswers = new ArrayList<>();
    private DiaryEntryModel currentDiaryEntry;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    /**
     * Clear the given answers.
     */
    public void clearGivenAnswers() {
        givenAnswers.clear();
    }

    /**
     * Save the slider answer provided by the user.
     * @param stepId the id of the step
     * @param questionId the id of the question
     * @param sliderValue the value of the slider
     */
    public void saveAnswer(int stepId, int questionId, int sliderValue) {
        for (int i = 0; i < givenAnswers.size(); i++) {
            GivenAnswerModel answer = givenAnswers.get(i);
            if (answer.getStepId() == stepId && answer.getQuestionId() == questionId) {
                givenAnswers.set(i, new GivenAnswerModel(stepId, questionId, sliderValue));
                return;
            }
        }
        givenAnswers.add(new GivenAnswerModel(stepId, questionId, sliderValue));
    }

    /**
     * Save the checkbox, radio or text answer provided by the user.
     * @param stepId the id of the step
     * @param questionId the id of the question
     * @param answerIds the ids of the selected answers
     * @param hasOtherText the String entered in the other field for checkbox or radio questions or
     *                     the text field for text questions
     */
    public void saveAnswer(int stepId, int questionId, @Nullable int[] answerIds, @Nullable String hasOtherText) {
        for (int i = 0; i < givenAnswers.size(); i++) {
            GivenAnswerModel answer = givenAnswers.get(i);
            if (answer.getStepId() == stepId && answer.getQuestionId() == questionId) {
                givenAnswers.set(i, new GivenAnswerModel(stepId, questionId, answerIds, hasOtherText));
                return;
            }
        }
        givenAnswers.add(new GivenAnswerModel(stepId, questionId, answerIds, hasOtherText));
    }

    /**
     * Get the answer given for the current question.
     * @param stepId the id of the step
     * @param questionId the id of the question
     * @return the model representing the given answer
     */
    public GivenAnswerModel getGivenAnswer(int stepId, int questionId) {
        for (GivenAnswerModel givenAnswer:givenAnswers) {
            if (givenAnswer.getStepId() == stepId && givenAnswer.getQuestionId() == questionId) {
                return givenAnswer;
            }
        }
        return null;
    }

    public List<GivenAnswerModel> getGivenAnswers() {
        return givenAnswers;
    }

    public QuestionModel getQuestion(int stepId, int questionId) {
        for (StepModel step:getSteps()) {
            if (step.getId() == stepId) {
                for (QuestionModel question:step.getQuestions()) {
                    if (question.getId() == questionId) {
                        return question;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<AnswerModel> getAnswersForQuestion(int stepId, int questionId) {
        QuestionModel question = getQuestion(stepId, questionId);
        if (question != null) {
            return question.getAnswers();
        }
        return null;
    }

    public List<StepModel> getSteps() {
        return steps;
    }

    public void setSteps(List<StepModel> steps) {
        this.steps = steps;
    }

    public DiaryEntryModel getCurrentDiaryEntry() {
        return currentDiaryEntry;
    }

    public void setCurrentDiaryEntry(DiaryEntryModel currentDiaryEntry) {
        this.currentDiaryEntry = currentDiaryEntry;
    }
}
