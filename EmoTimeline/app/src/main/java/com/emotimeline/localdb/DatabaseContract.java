package com.emotimeline.localdb;

import android.provider.BaseColumns;

/**
 * Represents the structure of the database.
 *
 * @author Kevin
 */
public final class DatabaseContract {
    static final String DATABASE_NAME = "database.db";
    static final int DATABASE_VERSION = 111;

    private DatabaseContract() {}

    /**
     * Represents the structure of the user table.
     */
    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "user";
        static final String KEY = "key";
        static final String COACH_IMAGE = "coachImage";
        static final String COACH_BEHAVIOUR = "coach_behaviour";
        static final String NAME = "name";
        static final String COACH_NAME = "coachName";
        static final String RECEIVES_NOTIFICATION = "receivesNotification";
        static final String NOTIFICATION_HOUR = "notificationHour";
        static final String NOTIFICATION_MINUTE = "notificationMinute";

        static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                KEY + " TEXT," +
                COACH_IMAGE + " BLOB," +
                COACH_NAME + " TEXT," +
                COACH_BEHAVIOUR + " TEXT," +
                NAME + " TEXT," +
                RECEIVES_NOTIFICATION + " INTEGER," +
                NOTIFICATION_HOUR + " INTEGER," +
                NOTIFICATION_MINUTE + " INTEGER" + " )";
        static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    /**
     * Represents the structure of the message table.
     */
    public static class Message implements BaseColumns {
        public static final String TABLE_NAME = "message";
        static final String MESSAGE = "message";
        static final String SENDER = "sender";
        static final String DATE = "date";
        static final String IS_SYNCED = "isSynced";
        static final String EMOTION = "emotion";
        static final String COACH_REACTION = "coach_reaction";

        static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                MESSAGE + " TEXT," +
                SENDER + " INTEGER," +
                DATE + " INTEGER," +
                IS_SYNCED + " INTEGER," +
                EMOTION + " TEXT," +
                COACH_REACTION + " TEXT" + " )";
        static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
