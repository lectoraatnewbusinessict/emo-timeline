package com.emotimeline.localdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Singleton which provides a helper class for database access.
 *
 * @author Kevin
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance;

    public DatabaseHelper(Context context) {
        super(context, DatabaseContract.DATABASE_NAME, null, DatabaseContract.DATABASE_VERSION);
    }

    /**
     * Get the instance of the helper.
     *
     * @param context the context that creates the instance
     * @return the current instance
     */
    static synchronized DatabaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DatabaseHelper(context);
        return instance;
    }

    /**
     * Create database tables.
     *
     * @param db the database that needs the tables
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContract.Users.CREATE_TABLE);
        db.execSQL(DatabaseContract.Message.CREATE_TABLE);
    }

    /**
     * Upgrade the database if built with an older version.
     *
     * @param db the updated database
     * @param oldVersion old version of the database
     * @param newVersion new version of the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseContract.Users.DELETE_TABLE);
        db.execSQL(DatabaseContract.Message.DELETE_TABLE);
        onCreate(db);
    }
}
