package com.emotimeline.localdb;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Represents a database entity.
 *
 * @author Kevin
 */
abstract class DBEntity {

    SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    protected Context context;

    /**
     * Creates an new DBEntity instance.
     *
     * @param context the context that uses the entity
     */
    DBEntity(Context context) {
        this.context = context;
        dbHelper = DatabaseHelper.getHelper(context);
        try {
            open();
        } catch (SQLException e) {
            Log.e("DBEntity", e.getMessage());
        }
    }

    /**
     * Open the database for usage.
     *
     * @throws SQLException thrown if database can not be opened
     */
    private void open() throws SQLException {
        if (dbHelper == null)
            dbHelper = DatabaseHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
    }


}
