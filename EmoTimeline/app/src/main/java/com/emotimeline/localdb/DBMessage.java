package com.emotimeline.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.emotimeline.models.localdb.Emotion;
import com.emotimeline.models.localdb.Message;

import java.util.ArrayList;

/**
 * Represents a message in the database.
 *
 * @author Kevin
 */
public class DBMessage extends DBEntity {

    /**
     * Creates a new DBMessage instance.
     *
     * @param context the context that uses the message
     */
    public DBMessage(Context context) {
        super(context);
    }

    /**
     * Save the message in the database.
     *
     * @param message the message that needs to be saved
     * @return a long given by insert statement
     */
    public long save(Message message) {
        if (message == null)
            return 0;

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Message.MESSAGE, message.getMessage());
        values.put(DatabaseContract.Message.SENDER, message.getSender().ordinal());
        values.put(DatabaseContract.Message.DATE, message.getDate());
        values.put(DatabaseContract.Message.IS_SYNCED, message.isSynced());
        values.put(DatabaseContract.Message.EMOTION, message.getEmotion());
        values.put(DatabaseContract.Message.COACH_REACTION, message.getCoachReaction());

        return db.insert(DatabaseContract.Message.TABLE_NAME, null, values);
    }

    /**
     * Update the message in the database.
     *
     * @param message the message that needs to be updated
     */
    public long update(Message message) {
        if (message == null)
            return 0;

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Message.MESSAGE, message.getMessage());
        values.put(DatabaseContract.Message.SENDER, message.getSender().ordinal());
        values.put(DatabaseContract.Message.DATE, message.getDate());
        values.put(DatabaseContract.Message.IS_SYNCED, message.isSynced());
        values.put(DatabaseContract.Message.EMOTION, message.getEmotion());
        values.put(DatabaseContract.Message.COACH_REACTION, message.getCoachReaction());

        return db.update(DatabaseContract.Message.TABLE_NAME, values, "_ID = ?", new String[]{ message.get_id().toString() });
    }

    /**
     * Retrieve the message from the database using its id.
     *
     * @return the message
     */
    public Message getMessageById(Integer id) {
        Cursor cursor = db.query(DatabaseContract.Message.TABLE_NAME,
                new String[] {
                        DatabaseContract.Message._ID,
                        DatabaseContract.Message.COACH_REACTION,
                        DatabaseContract.Message.EMOTION,
                        DatabaseContract.Message.DATE,
                        DatabaseContract.Message.IS_SYNCED,
                        DatabaseContract.Message.SENDER,
                        DatabaseContract.Message.MESSAGE
                },
                "_ID= ?", new String[]{ id.toString() }, null, null, null, null);

        if (cursor.getCount() == 0)
            return null;

        cursor.moveToFirst();

        return createMessageFromCursor(cursor, true);
    }

    /**
     * Retrieve all messages from the database.
     *
     * @return a list with all the messages
     */
    public ArrayList<Message> getMessages() {
        ArrayList<Message> messages = new ArrayList<>();

        Cursor cursor = db.query(DatabaseContract.Message.TABLE_NAME,
                new String[] {
                        DatabaseContract.Message.MESSAGE,
                        DatabaseContract.Message.SENDER,
                        DatabaseContract.Message.DATE,
                        DatabaseContract.Message.IS_SYNCED,
                        DatabaseContract.Message.EMOTION,
                        DatabaseContract.Message.COACH_REACTION },
                null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            messages.add(createMessageFromCursor(cursor, false));
        }
        cursor.close();

        return messages;
    }

    /**
     * Create message model from database cursor.
     *
     * @param cursor the cursor in the database
     * @param newMessage true if a new message needs to be created
     * @return the message model
     */
    private Message createMessageFromCursor(Cursor cursor, boolean newMessage) {
        Message message = new Message();

        if (newMessage) {
            message.set_id(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Message._ID)));
            message.setCoachBehaviour("meegaand");
            message.setSynced(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.IS_SYNCED))));
            message.setDate(Long.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.DATE))));
            message.setSender(Message.Sender.values()[Integer.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.SENDER)))]);
        } else {
            message.setCoachReaction(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.COACH_REACTION)));
            message.setSynced(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Message.IS_SYNCED)) == 1);
            message.setDate(cursor.getLong(cursor.getColumnIndex(DatabaseContract.Message.DATE)));
            message.setSender(Message.Sender.values()[cursor.getInt(cursor.getColumnIndex(DatabaseContract.Message.SENDER))]);
        }
        message.setMessage(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.MESSAGE)));
        message.setEmotion(cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.EMOTION)));

        return message;
    }

    /**
     * Retrieve all emotions from the database.
     *
     * @return a list with all the emotions
     */
    public ArrayList<Emotion> getEmotions(int weeks) {
        ArrayList<Emotion> emotions = new ArrayList<>();
        int period = weeks * 7 * 24 * 60 * 60;
        String whereClause = DatabaseContract.Message.DATE + " > "
                           + (System.currentTimeMillis() / 1000 - period)
                           + " AND " + DatabaseContract.Message.SENDER + " = "
                           + Message.Sender.USER.ordinal()
                           + " AND " + DatabaseContract.Message.EMOTION + " IS NOT NULL"
                           + " AND " + DatabaseContract.Message.EMOTION + " != ''";
        String groupClause = DatabaseContract.Message.EMOTION;

        Cursor cursor = db.query(DatabaseContract.Message.TABLE_NAME,
                new String[] { DatabaseContract.Message.EMOTION, "count()" },
                whereClause, null, groupClause, null, null, null);

        while (cursor.moveToNext()) {
            emotions.add(createEmotionFromCursor(cursor, false));
        }
        cursor.close();

        return emotions;
    }

    /**
     * Retrieve all emotions from the database sorted by days.
     *
     * @return a list with all the emotions
     */
    public ArrayList<Emotion> getEmotionsPerDay(int days) {
        ArrayList<Emotion> emotions = new ArrayList<>();
        int period = days * 24 * 60 * 60;
        String whereClause = DatabaseContract.Message.DATE + " > "
                + (System.currentTimeMillis() / 1000 - period)
                + " AND " + DatabaseContract.Message.EMOTION +
                " IS NOT NULL";
        String groupClause = DatabaseContract.Message.EMOTION + ",day";

        Cursor cursor = db.query(DatabaseContract.Message.TABLE_NAME,
                new String[] { DatabaseContract.Message.EMOTION,
                        "date(" + DatabaseContract.Message.DATE + ", 'unixepoch') as day",
                               "count()" },
                whereClause, null, groupClause, null, null, null);

        while (cursor.moveToNext()) {
            emotions.add(createEmotionFromCursor(cursor, true));
        }
        cursor.close();

        return emotions;
    }

    /**
     * Create emotion model from database cursor.
     *
     * @param cursor the cursor in the database
     * @param perDay true if emotions need to be sorted per day
     * @return the emotion model
     */
    private Emotion createEmotionFromCursor(Cursor cursor, boolean perDay) {
        if (perDay) {
            return new Emotion(
                    cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.EMOTION)),
                    cursor.getInt(cursor.getColumnIndex("count()")),
                    cursor.getString(cursor.getColumnIndex("day")));
        }
        return new Emotion(
                cursor.getString(cursor.getColumnIndex(DatabaseContract.Message.EMOTION)),
                cursor.getInt(cursor.getColumnIndex("count()")));
    }

    /**
     * Retrieve messages which were not synced.
     *
     * @return a list with all the messages
     */
    public ArrayList<Message> getNotSyncedMessages() {
        ArrayList<Message> messages = new ArrayList<>();

        String whereClause = DatabaseContract.Message.IS_SYNCED+" = "+ 0 + " AND " + DatabaseContract.Message.SENDER + "!="+ Message.Sender.COACH.ordinal();
        Log.d("where", whereClause);

        Cursor cursor = db.query(
                DatabaseContract.Message.TABLE_NAME,
                new String[] {
                        DatabaseContract.Message._ID,
                        DatabaseContract.Message.MESSAGE,
                        DatabaseContract.Message.SENDER,
                        DatabaseContract.Message.DATE,
                        DatabaseContract.Message.IS_SYNCED,
                        DatabaseContract.Message.EMOTION,
                        DatabaseContract.Message.COACH_REACTION
                },
                whereClause, null, null, null, null, null
        );

        while (cursor.moveToNext()) {
            messages.add(createMessageFromCursor(cursor, false));
        }
        cursor.close();

        return messages;
    }

    /**
     * Truncate the table.
     */
    public void truncate()
    {
        db.execSQL("delete from "+ DatabaseContract.Message.TABLE_NAME);
    }

}
