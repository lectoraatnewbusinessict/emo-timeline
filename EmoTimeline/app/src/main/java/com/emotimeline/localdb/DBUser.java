package com.emotimeline.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;

import com.emotimeline.models.localdb.User;
import com.emotimeline.receivers.AlarmReceiver;
import com.emotimeline.services.NotificationService;

/**
 * Represents a user in the database.
 *
 * @author Kevin
 */
public class DBUser extends DBEntity {

    /**
     * Creates a new DBUser instance.
     *
     * @param context the context that uses the user
     */
    public DBUser(Context context) {
        super(context);
    }

    /**
     * Save the user in the database.
     *
     * @param user the user that needs to be saved
     * @return a long given by insert statement
     */
    public long save(User user) {
        if (user == null)
            return 0;

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users._ID, user.get_id());
        values.put(DatabaseContract.Users.KEY, user.getKey());
        values.put(DatabaseContract.Users.NAME, user.getName());
        values.put(DatabaseContract.Users.COACH_IMAGE, user.getCoachImage());
        values.put(DatabaseContract.Users.COACH_NAME, user.getCoachName());
        values.put(DatabaseContract.Users.COACH_BEHAVIOUR, user.getCoach_behaviour());
        values.put(DatabaseContract.Users.RECEIVES_NOTIFICATION, user.isReceivesNotification());
        values.put(DatabaseContract.Users.NOTIFICATION_HOUR, user.getNotificationHour());
        values.put(DatabaseContract.Users.NOTIFICATION_MINUTE, user.getNotificationMinute());

        return db.insert(DatabaseContract.Users.TABLE_NAME, null, values);
    }

    /**
     * Update the user in the database.
     *
     * @param user the user that needs to be updated
     */
    public void update(User user) {
        if (user == null)
            return;

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users.KEY, user.getKey());
        values.put(DatabaseContract.Users.NAME, user.getName());
        values.put(DatabaseContract.Users.COACH_IMAGE, user.getCoachImage());
        values.put(DatabaseContract.Users.COACH_NAME, user.getCoachName());

        db.update(DatabaseContract.Users.TABLE_NAME, values, "_id="+user.get_id(), null);
    }

    /**
     * Update the name of the given user in the database.
     *
     * @param user the user who's name needs to be updated
     * @param name the new name for the user
     */
    public void updateName(User user, String name) {
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users.NAME, name);
        db.update(
                DatabaseContract.Users.TABLE_NAME,
                values, "_ID = ?",
                new String[]{ "" + user.get_id() }
        );
    }

    /**
     * Update notification settings for given user.
     *
     * @param context the context that updates the settings
     * @param user the user who's settings needs to be updated
     * @param receivesNotification does the user want to receive notifications
     * @param hour the hour at which the user wants to receive notifications
     * @param minute the minute at which the user wants to receive notifications
     */
    public void updateNotificationSettings(Context context, @Nullable User user, boolean receivesNotification,
                                           int hour, int minute) {
        if (user == null) {
            user = getUser();
        }

        NotificationService.setReminder(context, AlarmReceiver.class,
                hour, minute);

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users.RECEIVES_NOTIFICATION, receivesNotification);
        values.put(DatabaseContract.Users.NOTIFICATION_HOUR, hour);
        values.put(DatabaseContract.Users.NOTIFICATION_MINUTE, minute);
        db.update(
                DatabaseContract.Users.TABLE_NAME,
                values, "_ID = ?",
                new String[]{ "" + user.get_id() }
        );
    }

    /**
     * Retrieve the user from the database.
     *
     * @return the user
     */
    public User getUser() {
        Cursor cursor = db.query(DatabaseContract.Users.TABLE_NAME,
                new String[] {
                        DatabaseContract.Users._ID,
                        DatabaseContract.Users.KEY,
                        DatabaseContract.Users.COACH_IMAGE,
                        DatabaseContract.Users.COACH_NAME,
                        DatabaseContract.Users.COACH_BEHAVIOUR,
                        DatabaseContract.Users.NAME,
                        DatabaseContract.Users.RECEIVES_NOTIFICATION,
                        DatabaseContract.Users.NOTIFICATION_HOUR,
                        DatabaseContract.Users.NOTIFICATION_MINUTE},
                null, null, null, null, null, null);

        if (cursor.getCount() == 0)
            return null;

        cursor.moveToFirst();

        User user = new User();
        user.set_id(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Users._ID)));
        user.setKey(cursor.getString(cursor.getColumnIndex(DatabaseContract.Users.KEY)));
        user.setCoachImage(cursor.getBlob(cursor.getColumnIndex(DatabaseContract.Users.COACH_IMAGE)));
        user.setCoachName(cursor.getString(cursor.getColumnIndex(DatabaseContract.Users.COACH_NAME)));
        user.setCoach_behaviour(cursor.getString(cursor.getColumnIndex(DatabaseContract.Users.COACH_BEHAVIOUR)));
        user.setName(cursor.getString(cursor.getColumnIndex(DatabaseContract.Users.NAME)));
        user.setReceivesNotification(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Users.RECEIVES_NOTIFICATION)) == 1);
        user.setNotificationHour(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Users.NOTIFICATION_HOUR)));
        user.setNotificationMinute(cursor.getInt(cursor.getColumnIndex(DatabaseContract.Users.NOTIFICATION_MINUTE)));
        cursor.close();

        return user;
    }

    /**
     * Truncate the table.
     */
    public void truncate()
    {
        db.execSQL("delete from "+ DatabaseContract.Users.TABLE_NAME);
    }

}
