package com.emotimeline.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

/**
 * Manages the Time Picker Dialog's and provides their functionality
 */
public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    /**
     * creates a TimePicker and returns a timePicker when a Dialog is created.
     *
     * @param savedInstanceState Bundle of data about instance state
     * @return TimePickerDialog
     */
    @Override
    public TimePickerDialog onCreateDialog(Bundle savedInstanceState) {
        return new TimePickerDialog(getActivity(), this, 0, 0, true);
    }

    /**
     * When user selects a time, set it as the new time to send the daily notification.
     *
     * @param view      TimePicker view
     * @param hourOfDay Int new hour
     * @param minute    Int new minute
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }
}
