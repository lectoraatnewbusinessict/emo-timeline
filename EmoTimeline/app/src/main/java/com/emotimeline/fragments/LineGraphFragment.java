package com.emotimeline.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.emotimeline.R;
import com.emotimeline.models.localdb.Emotion;
import com.emotimeline.localdb.DBMessage;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Creates and manages a line graph.
 */
public class LineGraphFragment extends Fragment {
    View view;

    private LinearLayout lineGraphLayout;
    private LineChart lineChart;

    /**
     *
     * @param savedInstanceState Bundle of data about instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Creates view and a filled line chart.
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_linegraph, container, false);
        lineGraphLayout = view.findViewById(R.id.linegraph_layout);

        createLineChart();
        makeLegend();
        addData();

        return view;
    }

    /**
     * Creates and customizes a line chart.
     */
    private void createLineChart() {
        lineChart = new LineChart(view.getContext());
        lineGraphLayout.addView(lineChart);

        Description lineChartDescription = new Description();
        lineChartDescription.setText(getContext().getString(R.string.line_graph_description));
        lineChart.setDescription(lineChartDescription);
        lineChart.setNoDataText(getContext().getString(R.string.no_graph_data));
        lineChart.setMinimumHeight(1000);
        lineChart.setHighlightPerTapEnabled(true);
        lineChart.setTouchEnabled(true);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);
        lineChart.setPinchZoom(true);
    }

    /**
     *Creates graph legend.
     */
    private void makeLegend() {
        Legend legend = lineChart.getLegend();

        legend.setForm(Legend.LegendForm.LINE);
        legend.setTextSize(18f);

        XAxis xl = lineChart.getXAxis();
        xl.setTextColor(Color.BLACK);
        xl.setTextSize(12f);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);

        YAxis yl = lineChart.getAxisLeft();
        yl.setTextColor(Color.BLACK);
        yl.setTextSize(12f);
        yl.setAxisMaximum(120f);
        yl.setDrawGridLines(true);

        YAxis yl2 = lineChart.getAxisRight();
        yl2.setEnabled(false);
    }

    /**
     * Adds data to the line chart
     */
    private void addData() {
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS) {
            colors.add(c);
        }

        Map<String, List<Entry>> lines = convertEmotionsToGraphLines(new DBMessage(getContext()).getEmotionsPerDay(7));

        LineData lineData = createFilledLineDataSet(lines, colors);

        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    /**
     * Creates a line for every given Emotion
     * @param emotions ArrayList <Emotion>
     * @return map <String to List<Entry>>
     */
    private Map<String, List<Entry>> convertEmotionsToGraphLines(ArrayList<Emotion> emotions) {
        Map<String, List<Entry>> lines = new HashMap<>();
        for (Emotion emotion : emotions) {
            try {
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(emotion.getDate());
                if (lines.get(emotion.getName()) == null) {
                    lines.put(emotion.getName(), new ArrayList<Entry>());
                }
                lines.get(emotion.getName()).add(new Entry(date.getDay() ,emotion.getCount()));
            } catch (java.text.ParseException e) {
                Log.e("LineGraph failed parse", e.getMessage());
            }
        }
        return lines;
    }

    /**
     * Creates filled dataset with given Lines.
     *
     * @param lines  List <Entry>
     * @param colors ArrayList<Integer> with colors
     * @return LineData
     */
    private LineData createFilledLineDataSet(Map<String, List<Entry>> lines, ArrayList<Integer> colors) {
        LineData lineData = new LineData();
        int count = 0;
        for (String emotion : lines.keySet()) {
            LineDataSet dataSet = new LineDataSet(lines.get(emotion), emotion);
            dataSet.setValueTextSize(12f);
            dataSet.setColor(colors.get(count));
            dataSet.setValueTextColor(Color.BLACK);
            lineData.addDataSet(dataSet);
            count++;
        }
        return lineData;
    }
}
