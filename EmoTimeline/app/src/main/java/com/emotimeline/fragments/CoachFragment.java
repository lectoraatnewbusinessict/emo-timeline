package com.emotimeline.fragments;

import android.content.Intent;
import android.content.SharedPreferences;

import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.emotimeline.R;
import com.emotimeline.activities.MainActivity;
import com.emotimeline.activities.SyncActivity;

import com.emotimeline.models.localdb.User;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.utils.ImageConverter;

import java.io.IOException;

/**
 * Creates the Coach screen with the avatar, name and the attitude buttons
 */
public class CoachFragment extends Fragment implements SyncActivity, View.OnClickListener, View.OnFocusChangeListener {

    View view;
    View activityView;
    Button buttonSoft;
    Button buttonHard;
    ImageView coachAvatar;
    EditText coachName;

    /**
     * @param savedInstanceState Bundle of data about instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return View view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DBUser dbUser = new DBUser(getContext());

        view = inflater.inflate(R.layout.fragment_coach, container, false);

        activityView = inflater.inflate(R.layout.activity_main, container, false);
        setButtonListeners();
        setupCoach(dbUser);

        if (dbUser.getUser().getCoachImage() != null) {
            loadCoachImage();
        }
        return view;
    }

    /**
     * Syncactivity onResume()
     * When the synchronisation continues, the fragment will request focus en start listening again.
     */
    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Syncactivity finish()
     * On finishing sync, removes this fragment and starts the main activity.
     */
    private void finish() {
        getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    /**
     * Only one button can be active at a time
     * @param v View that contains clicked items
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_soft:
                buttonSoft.setEnabled(false);
                buttonHard.setEnabled(true);
                break;

            case R.id.button_hard:
                buttonHard.setEnabled(false);
                buttonSoft.setEnabled(true);
                break;
        }
    }

    /**
     * Selects an image.
     */
    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Complete action using"),1);
    }

    /**
     *
     * @param requestCode int Request code
     * @param resultCode int
     * @param data Intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    saveImage(selectedImageUri);
                }
                break;
        }
    }

    /**
     * Stores the image of the coach.
     * @param uri Uri
     */
    public void saveImage(Uri uri) {
        try {
            DBUser dbUser = new DBUser(getContext());
            User user = dbUser.getUser();
            user.setCoachImage(
                    ImageConverter.bitmapToBytes(
                            MediaStore.Images.Media.getBitmap(
                                    view.getContext().getContentResolver(),
                                    uri
                            )
                    )
            );
            dbUser.update(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        e.putBoolean("firstStartC", false);
        e.apply();

        ((MainActivity)getActivity()).setCoachData();

        loadCoachImage();
    }

    /**
     * Loads the stored image of the Coach from the db.
     */
    public void loadCoachImage() {
        coachAvatar.setImageBitmap(ImageConverter.getCroppedBitmap(ImageConverter.bytesToBitmap(
                new DBUser(getContext()).getUser().getCoachImage())
        ));
    }

    /**
     * TODO: Implement this part. Unclear what it needs to do.
     * Not implemented yet
     * @param response String
     * @param responseType class unknown
     */
    @Override
    public void performAfterSync(String response, Class<?> responseType) {
        throw new IllegalStateException("Not implemented yet");
    }

    /**
     * When the fragment receives the focus, setup the coach
     * @param v View
     * @param hasFocus boolean
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            DBUser dbUser = new DBUser(getContext());
            User user = dbUser.getUser();
            user.setCoachName(coachName.getText().toString());
            dbUser.update(user);

            MainActivity.hideSoftKeyboard(view);
            ((MainActivity)getActivity()).setCoachData();
        }
    }

    /**
     * Add click listeners to the buttons
     */
    private void setButtonListeners() {
        buttonSoft = view.findViewById(R.id.button_soft);
        buttonHard = view.findViewById(R.id.button_hard);

        buttonSoft.setOnClickListener(this);
        buttonHard.setOnClickListener(this);
    }

    /**
     * Sets up the coach.
     * @param dbUser DBUser containing user
     */
    private void setupCoach(DBUser dbUser) {
        coachAvatar = view.findViewById(R.id.coach_image);
        coachAvatar.setClickable(true);
        coachAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        coachName = view.findViewById(R.id.naamCoachveld);
        coachName.setText(dbUser.getUser().getCoachName());
        coachName.setOnFocusChangeListener(this);
    }
}
