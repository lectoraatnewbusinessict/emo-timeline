package com.emotimeline.fragments.questions;

import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.models.AnswerModel;
import com.emotimeline.models.GivenAnswerModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Represents the question where the user answers using a radio button.
 *
 * @author Kevin
 */
public class RadioQuestionFragment extends QuestionFragment {

    private boolean otherSelected = false;

    @BindView(R.id.radio_question_text) TextView questionTextView;
    @BindView(R.id.radio_optional_text) TextView optionalTextView;
    @BindView(R.id.radio_group) RadioGroup radioGroup;
    @BindView(R.id.radio_button_editable_layout) View otherRadioButtonView;
    @BindView(R.id.radio_button_editable) AppCompatRadioButton otherRadioButton;
    @BindView(R.id.radio_button_edit_text) EditText otherEditText;

    @Override
    public QuestionFragment getThis() {
        return new RadioQuestionFragment();
    }

    @Override
    public void setupViews() {
        questionTextView.setText(getQuestion().getText());

        if (getQuestion().getSkippable()) {
            optionalTextView.setVisibility(View.VISIBLE);
        }

        createRadioButtons();
    }

    @Override
    public void saveAnswer() {
        if (otherSelected && !TextUtils.isEmpty(otherEditText.getText().toString())) {
            AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                    null, otherEditText.getText().toString());
        } else {
            AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                    new int[]{radioGroup.getCheckedRadioButtonId()}, null);
        }
    }

    @Override
    public void applySavedAnswer() {
        GivenAnswerModel answer = AppController.getInstance().getGivenAnswer(getQuestion().getStepId(),
                getQuestion().getId());
        if (answer != null) {
            if (answer.getAnswerIds() != null && answer.getAnswerIds().length > 0) {
                radioGroup.check(answer.getAnswerIds()[0]);
            } else if (answer.getHasOtherText() != null) {
                otherSelected = true;
                otherRadioButton.setChecked(true);
                otherEditText.setText(answer.getHasOtherText());
            }
        }
    }

    @Override
    public boolean hasValidAnswer() {
        if (getQuestion().getHasOther()) {
            if (otherSelected && !TextUtils.isEmpty(otherEditText.getText().toString())) {
                return true;
            }
            if (otherSelected && TextUtils.isEmpty(otherEditText.getText().toString())) {
                return false;
            }
        }
        return radioGroup.getCheckedRadioButtonId() != -1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQuestionAndAnswers(getArguments().getInt("stepId"),
                getArguments().getInt("questionId"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio_question, container, false);
        ButterKnife.bind(this, view);

        setupViews();

        applySavedAnswer();

        return view;
    }

    /**
     * Get the answer selected by to user.
     * @return the selected answer
     */
    public AnswerModel getSelectedAnswer() {
        for (AnswerModel answer:getAnswers()) {
            if (answer.getId() == radioGroup.getCheckedRadioButtonId())
                return answer;
        }
        return null;
    }

    /**
     * Create radio buttons using the answers from the API.
     */
    private void createRadioButtons() {
        int numberOfRadioButtons = getAnswers().size();
        if (getQuestion().getHasOther()) {
            numberOfRadioButtons++;
        }

        final AppCompatRadioButton[] radioButtons = new AppCompatRadioButton[numberOfRadioButtons];
        for (int i = 0; i < getAnswers().size(); i++) {
            AnswerModel answer = getAnswers().get(i);
            radioButtons[i] = (AppCompatRadioButton) LayoutInflater.from(getActivity())
                    .inflate(R.layout.radio_button_custom, null);
            radioButtons[i].setId(answer.getId());
            radioButtons[i].setText(answer.getText());
            radioGroup.addView(radioButtons[i]);
        }

        if (getQuestion().getHasOther()) {
            showOtherRadioButton();
        }
    }

    /**
     * Show the radio button with an EditText.
     */
    private void showOtherRadioButton() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() != -1) {
                    otherRadioButton.setChecked(false);
                } else {
                    otherRadioButton.setChecked(true);
                }
            }
        });

        otherRadioButtonView.setVisibility(View.VISIBLE);
        otherRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                otherSelected = b;
                if (b) {
                    radioGroup.clearCheck();
                }
            }
        });
    }
}