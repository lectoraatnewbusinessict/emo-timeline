package com.emotimeline.fragments.questions;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.models.GivenAnswerModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Represents the question where the user answers using a text field.
 *
 * @author Kevin
 */
public class TextQuestionFragment extends QuestionFragment {

    @BindView(R.id.text_question_text) TextView questionTextView;
    @BindView(R.id.text_optional_text) TextView optionalTextView;
    @BindView(R.id.edit_text) MaterialEditText editText;

    @Override
    public QuestionFragment getThis() {
        return new TextQuestionFragment();
    }

    @Override
    public void setupViews() {
        questionTextView.setText(getQuestion().getText());

        if (getQuestion().getSkippable()) {
            optionalTextView.setVisibility(View.VISIBLE);
        }

        if (getQuestion().getAnswers().get(0).getMinCharacters() != 0) {
            editText.setMinCharacters(getQuestion().getAnswers().get(0).getMinCharacters());
        }
        if (getQuestion().getAnswers().get(0).getMaxCharacters() != 0) {
            editText.setMaxCharacters(getQuestion().getAnswers().get(0).getMaxCharacters());
        }

        editText.setHint(getQuestion().getAnswers().get(0).getPlaceholder());
    }

    @Override
    public void saveAnswer() {
        AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                null, editText.getText().toString());
    }

    @Override
    public void applySavedAnswer() {
        GivenAnswerModel answer = AppController.getInstance().getGivenAnswer(getQuestion().getStepId(),
                getQuestion().getId());
        if (answer != null && answer.getHasOtherText() != null) {
            editText.setText(answer.getHasOtherText());
        }
    }

    @Override
    public boolean hasValidAnswer() {
        return editText.isCharactersCountValid() && !TextUtils.isEmpty(editText.getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQuestionAndAnswers(getArguments().getInt("stepId"),
                getArguments().getInt("questionId"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text_question, container, false);
        ButterKnife.bind(this, view);

        setupViews();

        applySavedAnswer();

        return view;
    }
}
