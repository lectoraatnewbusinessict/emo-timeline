package com.emotimeline.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.emotimeline.R;
import com.emotimeline.activities.MainActivity;
import com.emotimeline.adapters.MessageAdapter;
import com.emotimeline.helpers.SnackbarHelper;
import com.emotimeline.localdb.DBMessage;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.Message;
import com.emotimeline.models.localdb.User;
import com.emotimeline.models.parameters.SendMessage;
import com.emotimeline.models.parameters.SendMessageParameter;
import com.emotimeline.models.response.MessageResponse;
import com.emotimeline.models.response.RegisterResponse;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;
import com.emotimeline.receivers.AlarmReceiver;
import com.emotimeline.services.MessageService;
import com.emotimeline.services.NotificationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Creates and manages message timeline.
 *
 * @author Kevin
 */
public class TimelineFragment extends AbstractFragment implements View.OnClickListener,
        TextView.OnEditorActionListener, View.OnFocusChangeListener {

    private EditText messageField;
    private View view;
    private RecyclerView recyclerView;
    private static final String COACH_BEHAVIOUR = "meegaand";
    private boolean alertSeen = false;
    private Timer updateMessageTimeTimer;
    public static ArrayList<Message> messages;
    public static MessageAdapter messageAdapter;

    /**
     * Fragment onCreate method in which the messages are initialized.
     *
     * @param savedInstanceState Bundle of data about instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messages = new DBMessage(getContext()).getMessages();
        messageAdapter = new MessageAdapter(messages);
    }

    /**
     * Fragment onStart with a check for first start. If the method is called for the first time,
     * register the user or show the welcoming message.
     */
    public void onStart() {
        super.onStart();
        if (MainActivity.firstStart) {
            startApplication();
            MainActivity.firstStart = false;
        }
    }

    /**
     * Fragment onCreateView which inflates the view and initializes the timer.
     *
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return the inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_timeline, container, false);
        makeButtonsAndFields();

        if (updateMessageTimeTimer != null) {
            updateMessageTimeTimer.cancel();
            updateMessageTimeTimer = null;
        }
        updateMessageTimeTimer = new Timer();
        updateMessageTimeTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageAdapter.notifyDataSetChanged();
                    }
                });
            }
        }, 60000, 60000);
        return view;
    }

    /**
     * Create buttons and fields for fragment.
     */
    private void makeButtonsAndFields() {
        Button messageSubmitButton = view.findViewById(R.id.messageSubmitButton);
        messageSubmitButton.setOnClickListener(this);

        messageField = view.findViewById(R.id.messageField);
        messageField.setOnEditorActionListener(this);
        messageField.setOnFocusChangeListener(this);

        recyclerView = view.findViewById(R.id.messages_container);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext());
        linearLayout.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.getItemAnimator().setAddDuration(650);
        recyclerView.setAdapter(messageAdapter);
    }


    /**
     * ClickListener for entire view. If the submit button is clicked, send the message.
     *
     * @param v the view which was clicked
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.messageSubmitButton:
                sendMessage();
                break;
        }
    }

    /**
     * Decide whether a keyboard action is allowed or not. If action send is performed, send the
     * message.
     *
     * @param v text view which is edited
     * @param actionId the is for the performed action
     * @param event the key event performing the action
     * @return whether the action is allowed or not
     */
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            sendMessage();
            handled = true;
        }
        return handled;
    }

    /**
     * Sends messages to database.
     */
    public void sendMessage() {
        messageField = view.findViewById(R.id.messageField);

        String messageText = messageField.getText().toString();
        if (messageText.isEmpty())
            return;

        Message message = new Message(messageText, Message.Sender.USER);
        message.setCoachBehaviour(COACH_BEHAVIOUR);

        messages.add(message);

        Long id = sendMessageToLocalDatabase(message);
        message.set_id(id.intValue());

        sendMessageToDatabase(message);

        messageField.setText("");
    }

    /**
     * Save the new message locally.
     *
     * @param message the new message
     * @return the id for the new message
     */
    private long sendMessageToLocalDatabase(Message message) {
        int position = messages.size() - 1;
        messageAdapter.notifyItemInserted(position);
        recyclerView.smoothScrollToPosition(position);
        return new DBMessage(getContext()).save(message);
    }

    /**
     * API call to send the message.
     *
     * @param message the message that needs to be send
     */
    private void sendMessageToDatabase(Message message) {
        SendMessage sendMessage = new SendMessage(message.get_id(), message.getDate(),
                message.getMessage(), message.getCoachBehaviour());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        SendMessageParameter sendMessageParameter = new SendMessageParameter(
                Collections.singletonList(sendMessage), new DBUser(getContext()).getUser().getKey());
        Call<MessageResponse> call = apiService.sendMessage(sendMessageParameter);
        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(@NonNull Call<MessageResponse> call,
                                   @NonNull Response<MessageResponse> response) {
                if (response.code() == 200) {
                    MessageService.syncMessages(getContext(), response.body());
                }
            }
            @Override
            public void onFailure(@NonNull Call<MessageResponse> call, @NonNull Throwable t) {
            }
        });
    }

    /**
     * Called when the fragment onStart is called for the first time. If there is no user, register
     * a new user. If the welcoming message has not been seen yet, show it.
     */
    private void startApplication() {
        User user = new DBUser(getContext()).getUser();

        if (user == null) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<RegisterResponse> call = apiService.register();
            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(@NonNull Call<RegisterResponse> call,
                                       @NonNull Response<RegisterResponse> response) {
                    if (response.code() == 200) {
                        createUser(response.body().getId(), response.body().getKey());
                        Message message = new Message(response.body().getMessage(),
                                Message.Sender.COACH);
                        new DBMessage(getContext()).save(message);
                    } else {
                        showUserRegistrationError();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                    showUserRegistrationError();
                }
            });
        } else {
            if( !alertSeen ) {
                showAlert(view);
                alertSeen = true;
            }
        }
    }

    /**
     * Called when registration failed.
     */
    private void showUserRegistrationError() {
        SnackbarHelper.showErrorSnackbar(
                getActivity().findViewById(android.R.id.content),
                R.string.user_registration_error, R.string.retry,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startApplication();
                    }
                });
    }

    /**
     * Called when focus on view changed. If focus is gone, hide soft keyboard.
     *
     * @param v view with focus
     * @param hasFocus tells whether view has focus or not
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            MainActivity.hideSoftKeyboard(v);
        }
    }

    /**
     * Fragment onDestroy which cancels the reload message timer.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        updateMessageTimeTimer.cancel();
    }

    /**
     * Create a user and save it locally.
     *
     * @param id the is for the user
     * @param key the key for the user
     */
    private void createUser(int id, String key) {
        User user = new User();
        user.set_id(id);
        user.setKey(key);
        user.setReceivesNotification(true);
        user.setNotificationHour(18);
        user.setNotificationMinute(0);
        new DBUser(getContext()).save(user);

        NotificationService.setReminder(getActivity(), AlarmReceiver.class,
                user.getNotificationHour(), user.getNotificationMinute());
    }
}
