package com.emotimeline.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.emotimeline.R;
import com.emotimeline.activities.MainActivity;
import com.emotimeline.helpers.SnackbarHelper;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This fragment captures and reads QR-codes through the built-in back camera.
 * This is then used to transfer data.
 */
public class CaptureFragment extends AbstractFragment implements ActivityCompat.OnRequestPermissionsResultCallback {

    private View view;
    private SurfaceView cameraView;
    private CameraSource cameraSource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_readqr, container, false);
        cameraView = view.findViewById(R.id.camera_view);

        if (checkAndRequestPermissions()) {
            createQRReader();
        }
        return view;
    }

    /**
     * Creates QR reader with new thread.
     */
    private void createQRReader() {
        final BarcodeDetector QRReader = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = createCameraSource(QRReader);

        cameraSurfaceViewAddHolderFunctions(cameraSource);

        createBarcodeDetectorThread(QRReader, cameraSource);
    }

    /**
     * @param detector BarcodeDetector
     * @return CameraSource
     */
    private CameraSource createCameraSource(BarcodeDetector detector) {
        return new CameraSource
                .Builder(getActivity(), detector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(Boolean.TRUE)
                .build();
    }

    /**
     * Adds functions for creation, changing and destruction of camera surface
     *
     * @param cameraSource camera source
     */
    private void cameraSurfaceViewAddHolderFunctions(final CameraSource cameraSource) {
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    int cameraPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
                    if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                        ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
                    }
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });
    }

    /**
     * Creates and starts thread for BarcodeDetector.
     *
     * @param detector Barcode Detector
     * @param cameraSource Camera source
     */
    private void createBarcodeDetectorThread(final BarcodeDetector detector, final CameraSource cameraSource) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                detector.setProcessor(new Detector.Processor<Barcode>() {
                    @Override
                    public void release() {
                    }

                    @Override
                    public void receiveDetections(Detector.Detections<Barcode> detections) {
                        handleQRCodeDetection(detections, detector, cameraSource);
                    }
                });
            }
        }).start();
    }

    /**
     * Launches an instance of TransferFragment and swithces to it.
     *
     * @param jsonString Data to transfer in Json form
     */
    private void launchTransferFragment(String jsonString) {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.putExtra("displayFragment", R.id.transfering_fragment_layout);
        intent.putExtra("jsonString", jsonString);
        getContext().startActivity(intent);
    }

    /**
     * Passes information from detected QR and releases detector and camera.
     *
     * @param detections detected data by Detector
     * @param QRDetector BarcodeDetector
     * @param cameraSource Camera source
     */
    private void handleQRCodeDetection(Detector.Detections<Barcode> detections,
                                       BarcodeDetector QRDetector, CameraSource cameraSource) {
        final SparseArray<Barcode> barcodes = detections.getDetectedItems();

        if (barcodes.size() != 0) {
            String value = barcodes.valueAt(0).displayValue;
            Log.d("detection!", "Found detection, value of QR = " + value);
            launchTransferFragment(value);
            QRDetector.release();
            cameraSource.release();
        }
    }

    /**
     * Checks all required permissions and requests any missing ones.
     *
     * @return Boolean: True = all requested permissions granted.
     */
    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
            return false;
        }
        return true;
    }

    /**
     * Called when given permissions are clear.
     *
     * @param requestCode int
     * @param permissions String array of requested permissions
     * @param grantResults int array of granted permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted = requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
        if (granted) {
            createQRReader();
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        } else {
            View v = getActivity().findViewById(android.R.id.content);
            SnackbarHelper.showErrorSnackbar(v, R.string.QR_reader_permissions_denied, -1, null);
        }
    }

}
