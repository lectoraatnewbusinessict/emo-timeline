package com.emotimeline.fragments;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import com.emotimeline.R;


public abstract class AbstractFragment extends Fragment
{
    /**
     * Shows popup welcoming message
     * @param view view to display message
     */
    public void showAlert(View view){
        ImageView image = new ImageView(view.getContext());
        AlertDialog.Builder builder =
                new AlertDialog.Builder(view.getContext()).
                        setMessage(R.string.welcomeUserReturnedMessage).
                        setPositiveButton("Ok!", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.dismiss();
                            }
                        }).
                        setView(image);
        builder.create().show();
    }

}
