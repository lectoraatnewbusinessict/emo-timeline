package com.emotimeline.fragments.questions;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.emotimeline.AppController;
import com.emotimeline.models.AnswerModel;
import com.emotimeline.models.QuestionModel;

import java.util.ArrayList;

/**
 * This class holds the common functionality of a Fragment used for a question.
 *
 * @author Kevin
 */
public abstract class QuestionFragment extends Fragment {

    private QuestionModel question;
    private ArrayList<AnswerModel> answers;

    /**
     * Get the instance of the child Fragment.
     * @return the instance
     */
    public abstract QuestionFragment getThis();

    /**
     * Method with all setup needed for the views.
     */
    public abstract void setupViews();

    /**
     * Save the answer provided by the user.
     */
    public abstract void saveAnswer();

    /**
     * Check is there is a answer saved for the question and apply it.
     */
    public abstract void applySavedAnswer();

    /**
     * Check if the provided answer is valid.
     */
    public abstract boolean hasValidAnswer();

    /**
     * Create a new instance of the required child question and add the arguments to the bundle.
     *
     * @param stepId the id of the step the question is in
     * @param questionId the id of the question
     * @return a child question instance
     */
    public QuestionFragment newInstance(int stepId, int questionId) {
        QuestionFragment questionFragment = getThis();

        Bundle args = new Bundle();
        args.putInt("stepId", stepId);
        args.putInt("questionId", questionId);
        questionFragment.setArguments(args);

        return questionFragment;
    }

    /**
     * Get the required models so they can be used within the fragment.
     *
     * @param stepId the id of the step the question is in
     * @param questionId the id of the question
     */
    public void getQuestionAndAnswers(int stepId, int questionId) {
        setQuestion(AppController.getInstance().getQuestion(stepId, questionId));
        setAnswers(AppController.getInstance().getAnswersForQuestion(stepId, questionId));
    }

    public QuestionModel getQuestion() {
        return question;
    }

    public void setQuestion(QuestionModel question) {
        this.question = question;
    }

    public ArrayList<AnswerModel> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerModel> answers) {
        this.answers = answers;
    }
}
