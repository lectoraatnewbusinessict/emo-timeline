package com.emotimeline.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emotimeline.R;
import com.emotimeline.activities.DiaryEntryActivity;
import com.emotimeline.adapters.DiaryOverviewAdapter;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.DiaryEntryModel;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.animators.FadeInDownAnimator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment for the overview of diary entries.
 *
 * @author Peter
 */
public class DiaryOverviewFragment extends Fragment {

    @BindView(R.id.diary_entries_overview) RecyclerView recyclerView;
    @BindView(R.id.diary_fab) FloatingActionButton fab;
    @BindView(R.id.diary_entries_refresh) SwipeRefreshLayout swipeRefreshLayout;
    @OnClick(R.id.diary_fab) void onClickFab(){ openLogEntry();}

    private List<DiaryEntryModel> entries;
    private DiaryOverviewAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    /**
     * During the creation of the view, add a RecyclerView
     * @param inflater inflates the View
     * @param container container for the View
     * @param savedInstanceState state of the instance
     * @return Fragment DiaryOverview
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diaryoverview, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new FadeInDownAnimator());

        swipeRefreshLayout.setProgressViewOffset(false, -60, 160);
        swipeRefreshLayout.setColorSchemeResources(R.color.color_accent, R.color.color_primary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        return view;
    }

    /**
     * When the user clicks on the fab, it opens a new DiaryEntryActivity
     */
    private void openLogEntry() {
        Intent intent = new Intent(getActivity(), DiaryEntryActivity.class);
        startActivity(intent);
    }

    /**
     * Adds an adapter to the recycler and set the visibility of the recyclerview to visible.
     */
    private void initAdapter() {
        adapter = new DiaryOverviewAdapter(entries);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * Gets the data via de API and updates ui with new data
     */
    private void getData() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(true);
        }
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<DiaryEntryModel>> call = apiService.getDiaryEntries(new DBUser(getContext()).getUser().getKey());
        call.enqueue(new Callback<List<DiaryEntryModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<DiaryEntryModel>> call,
                                   @NonNull Response<List<DiaryEntryModel>> response) {
                if (response.code() == 200) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (entries == null) {
                        entries = response.body();
                        initAdapter();
                    } else {
                        if (response.body() != null) {
                            if (response.body().size() == entries.size()+1) {
                                entries.add(0, response.body().get(0));
                                adapter.notifyItemInserted(0);
                                recyclerView.getLayoutManager().scrollToPosition(0);
                            } else if (response.body().size() > entries.size()+1) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    setFailedMessage();
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<DiaryEntryModel>> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                setFailedMessage();
            }
        });
    }

    /**
     * Set a failed message in the snackbar, when user clicks try again try to get data again.
     */
    private void setFailedMessage() {
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                getString(R.string.prompt_diary_entries_failed), Snackbar.LENGTH_LONG).setAction(
                        getString(R.string.retry), new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        getData();
                    }
                }
        );
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
