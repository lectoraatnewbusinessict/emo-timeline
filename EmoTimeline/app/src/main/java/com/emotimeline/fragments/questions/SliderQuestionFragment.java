package com.emotimeline.fragments.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.models.GivenAnswerModel;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Represents the question where the user answers using a slider.
 *
 * @author Kevin
 */
public class SliderQuestionFragment extends QuestionFragment {

    @BindView(R.id.slider_question_text) TextView questionTextView;
    @BindView(R.id.slider_optional_text) TextView optionalTextView;
    @BindView(R.id.seek_bar) DiscreteSeekBar seekBar;
    @BindView(R.id.min_label) TextView minLabel;
    @BindView(R.id.max_label) TextView maxLabel;

    @Override
    public QuestionFragment getThis() {
        return new SliderQuestionFragment();
    }

    @Override
    public void setupViews() {
        questionTextView.setText(getQuestion().getText());

        if (getQuestion().getSkippable()) {
            optionalTextView.setVisibility(View.VISIBLE);
        }

        minLabel.setText(getQuestion().getAnswers().get(0).getMinText());
        maxLabel.setText(getQuestion().getAnswers().get(0).getMaxText());
    }

    @Override
    public void saveAnswer() {
        AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                seekBar.getProgress());
    }

    @Override
    public void applySavedAnswer() {
        GivenAnswerModel answer = AppController.getInstance().getGivenAnswer(getQuestion().getStepId(),
                getQuestion().getId());
        if (answer != null) {
            seekBar.setProgress(answer.getSliderValue());
        }
    }

    @Override
    public boolean hasValidAnswer() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQuestionAndAnswers(getArguments().getInt("stepId"),
                getArguments().getInt("questionId"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slider_question, container, false);
        ButterKnife.bind(this, view);

        setupViews();

        applySavedAnswer();

        return view;
    }
}
