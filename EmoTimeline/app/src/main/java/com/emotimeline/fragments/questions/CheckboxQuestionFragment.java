package com.emotimeline.fragments.questions;

import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.models.AnswerModel;
import com.emotimeline.models.GivenAnswerModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Represents the question where the user answers using a checkbox.
 *
 * @author Kevin
 */
public class CheckboxQuestionFragment extends QuestionFragment {

    List<Integer> checkedIds = new ArrayList<>();

    @BindView(R.id.checkbox_question_text) TextView questionTextView;
    @BindView(R.id.checkbox_optional_text) TextView optionalTextView;
    @BindView(R.id.checkbox_layout) LinearLayout checkBoxLayout;
    @BindView(R.id.check_box_editable_layout) View checkboxEditableView;
    @BindView(R.id.check_box_editable) AppCompatCheckBox otherCheckBox;
    @BindView(R.id.check_box_edit_text) EditText otherEditText;

    @Override
    public QuestionFragment getThis() {
        return new CheckboxQuestionFragment();
    }

    @Override
    public void setupViews() {
        questionTextView.setText(getQuestion().getText());

        if (getQuestion().getSkippable()) {
            optionalTextView.setVisibility(View.VISIBLE);
        }

        createCheckBoxes();
    }

    @Override
    public void saveAnswer() {
        if (otherCheckBox.isChecked() && !TextUtils.isEmpty(otherEditText.getText().toString())) {
            AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                    toIntArray(checkedIds), otherEditText.getText().toString());
        } else {
            AppController.getInstance().saveAnswer(getQuestion().getStepId(), getQuestion().getId(),
                    toIntArray(checkedIds), null);
        }
    }

    @Override
    public void applySavedAnswer() {
        GivenAnswerModel answer = AppController.getInstance().getGivenAnswer(getQuestion().getStepId(),
                getQuestion().getId());
        if (answer != null) {
            if (answer.getHasOtherText() != null) {
                otherCheckBox.setChecked(true);
                otherEditText.setText(answer.getHasOtherText());
            }
        }
    }

    @Override
    public boolean hasValidAnswer() {
        if (getQuestion().getHasOther()) {
            if (otherCheckBox.isChecked() && !TextUtils.isEmpty(otherEditText.getText().toString())) {
                return true;
            }
            if (otherCheckBox.isChecked() && TextUtils.isEmpty(otherEditText.getText().toString())) {
                return false;
            }
        }
        return checkedIds.size() > 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQuestionAndAnswers(getArguments().getInt("stepId"),
                getArguments().getInt("questionId"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkbox_question, container, false);
        ButterKnife.bind(this, view);

        setupViews();

        applySavedAnswer();

        return view;
    }
    /**
     * Create checkboxes using the answers from the API.
     */
    private void createCheckBoxes() {
        int numberOfCheckBoxes = getAnswers().size();
        if (getQuestion().getHasOther()) {
            numberOfCheckBoxes++;
        }

        AppCompatCheckBox[] checkBoxes = new AppCompatCheckBox[numberOfCheckBoxes];
        for (int i = 0; i < getAnswers().size(); i++) {
            AnswerModel answer = getAnswers().get(i);
            checkBoxes[i] = (AppCompatCheckBox) LayoutInflater.from(getActivity())
                    .inflate(R.layout.check_box_custom, null);
            checkBoxes[i].setId(answer.getId());
            checkBoxes[i].setText(answer.getText());
            checkBoxes[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        checkedIds.add(compoundButton.getId());
                    } else {
                        for (Iterator<Integer> iter = checkedIds.listIterator(); iter.hasNext(); ) {
                            Integer a = iter.next();
                            if (a == compoundButton.getId()) {
                                iter.remove();
                            }
                        }
                    }
                }
            });
            GivenAnswerModel givenAnswer = AppController.getInstance().getGivenAnswer(getQuestion().getStepId(),
                    getQuestion().getId());
            if (givenAnswer != null) {
                if (givenAnswer.getAnswerIds() != null && givenAnswer.getAnswerIds().length > 0) {
                    if (arrayContainsInt(givenAnswer.getAnswerIds(), checkBoxes[i].getId())) {
                        checkBoxes[i].setChecked(true);
                    }
                }
            }
            checkBoxLayout.addView(checkBoxes[i]);
        }

        if (getQuestion().getHasOther()) {
            showOtherCheckBox();
        }
    }

    /**
     * Show check box with an EditText.
     */
    private void showOtherCheckBox() {
        checkboxEditableView.setVisibility(View.VISIBLE);
    }

    /**
     * Check if an array contains a certain int.
     * @param arr the array
     * @param i the int
     * @return whether or not the int is found
     */
    private boolean arrayContainsInt(int[] arr, int i) {
        for (int anArr : arr) {
            if (anArr == i) {
                return true;
            }
        }
        return false;
    }

    /**
     * Change a list to an array.
     * @param list the list
     * @return an array
     */
    private int[] toIntArray(List<Integer> list)  {
        int[] ret = new int[list.size()];
        int i = 0;
        for (Integer e : list)
            ret[i++] = e;
        return ret;
    }
}