package com.emotimeline.fragments.slides;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.emotimeline.R;
import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;
import com.github.paolorotolo.appintro.ISlidePolicy;

/**
 * Provides user with a mandatory privacy agreement to sign
 */
public class PrivacyFragment extends Fragment implements ISlidePolicy, ISlideBackgroundColorHolder {
    private LinearLayout layoutContainer;
    private CheckBox checkBox;

    /**
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return View view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.privacyslide, container, false);

        layoutContainer = view.findViewById(R.id.slide_policy_demo_container);
        checkBox = view.findViewById(R.id.policy_check);
        setThisViewNoLongerShown();
        return view;
    }

    /**
     * @return Colour
     */
    @Override
    public int getDefaultBackgroundColor() {
        return Color.parseColor((Integer.toString(R.color.color_default)));
    }

    /**
     * @param backgroundColor Int
     */
    @Override
    public void setBackgroundColor(@ColorInt int backgroundColor) {
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
        }
    }

    /**
     * Checks if the user checked the agreement box
     *
     * @return boolean
     */
    @Override
    public boolean isPolicyRespected() {
        return checkBox == null || checkBox.isChecked();
    }

    /**
     * Throws a message to the user about refusing access to the next page.
     */
    @Override
    public void onUserIllegallyRequestedNextPage() {
        Toast.makeText(getContext(), R.string.slide_policy_demo_error, Toast.LENGTH_SHORT).show();
    }

    /**
     * Sets option to skip this view when it is called.
     */
    public void setThisViewNoLongerShown() {
        checkBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        SharedPreferences getPrefs = PreferenceManager
                                .getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor e = getPrefs.edit();
                        e.putBoolean("firstStart", !isChecked);
                        e.apply();
                    }
                }
        );
    }
}
