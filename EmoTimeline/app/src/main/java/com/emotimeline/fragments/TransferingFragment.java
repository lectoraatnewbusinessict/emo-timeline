package com.emotimeline.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emotimeline.R;
import com.emotimeline.localdb.DBMessage;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.Message;
import com.emotimeline.models.localdb.User;
import com.emotimeline.models.parameters.TransferDataParameter;
import com.emotimeline.models.response.TransferDataMessage;
import com.emotimeline.models.response.TransferDataResponse;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Transfers data and displays progress
 */
public class TransferingFragment extends Fragment {

    private ProgressBar progressBar;
    private TextView progressText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfering, container, false);

        progressBar = view.findViewById(R.id.progressBar);
        progressText = view.findViewById(R.id.progressText);

        synchronizeData(getActivity().getIntent().getStringExtra("jsonString"));

        return view;
    }

    /**
     * @param barcodeValue String barcode to transfer
     */
    private synchronized void synchronizeData(String barcodeValue) {
        TransferDataParameter transferDataParameter = verify(barcodeValue);
        callDatabase(transferDataParameter);
    }

    /**
     * @param transfer Object to transfer the key
     */
    private void callDatabase(final TransferDataParameter transfer) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TransferDataResponse> call = apiService.transferData(transfer);
        call.enqueue(new Callback<TransferDataResponse>() {
            @Override
            public void onResponse(@NonNull Call<TransferDataResponse> call,
                                   @NonNull Response<TransferDataResponse> response) {
                if (response.code() == 200) {
                    progressBar.setProgress(25);

                    if (response.body().isResult()) {
                        beginTransfer(response.body());
                    } else {
                        setIncorrectBarcode();
                    }
                } else {
                    setIncorrectBarcode();
                }
            }
            @Override
            public void onFailure(@NonNull Call<TransferDataResponse> call, @NonNull Throwable t) {
                setIncorrectBarcode();
            }
        });
    }

    /**
     * Show user bar code is incorrect.
     */
    private void setIncorrectBarcode() {
        progressBar.setVisibility(View.INVISIBLE);
        progressText.setTextSize(20);
        progressText.setText(R.string.Barcode_incorrect);
    }

    /**
     * Verifies Barcode
     *
     * @param barcodeValue String Barcode to verify
     * @return returns TransferDataParameter object with barcode
     */
    private TransferDataParameter verify(String barcodeValue) {
        return new TransferDataParameter(barcodeValue);
    }

    /**
     * @param transferResponse Repsonse containing the message
     */
    private void beginTransfer(TransferDataResponse transferResponse) {
        DBUser dbUser = new DBUser(getContext());
        DBMessage dbMessage = new DBMessage(getContext());

        progressBar.setProgress(50);

        dbMessage.truncate();
        dbUser.truncate();

        progressBar.setProgress(75);
        createUser(dbUser, transferResponse);

        saveMessage(dbMessage, transferResponse);

        progressBar.setProgress(100);
        progressText.setText(R.string.synchronization_success);
    }

    /**
     * @param dbUser           Container to store user in.
     * @param transferResponse Response containing user data.
     */
    private void createUser(DBUser dbUser, TransferDataResponse transferResponse) {
        User user = new User();
        user.setKey(transferResponse.getHash());
        user.set_id(transferResponse.getId());
        dbUser.save(user);
    }

    /**
     * @param dbMessage        Container used to store the message
     * @param transferResponse Response containing message
     */
    private void saveMessage(DBMessage dbMessage, TransferDataResponse transferResponse) {
        for (TransferDataMessage transferDataMessage : transferResponse.getMessages()) {
            Message message = new Message(transferDataMessage.getMessageId(),
                    transferDataMessage.getEmotion(), transferDataMessage.getDate(),
                    transferDataMessage.getMessage(), transferDataMessage.getCoachBehaviour());
            if (message.getCoachReaction() != null && !message.getCoachReaction().isEmpty()) {
                Message coachMessage = new Message(message.getCoachReaction(), Message.Sender.COACH);
                dbMessage.save(coachMessage);
            }

            message.setSender(Message.Sender.USER);
            dbMessage.save(message);
        }
    }
}
