package com.emotimeline.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.emotimeline.R;
import com.emotimeline.helpers.SnackbarHelper;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.parameters.PrepareTransferParameter;
import com.emotimeline.models.response.PrepareTransferResponse;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;
import com.emotimeline.utils.Encryption;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Prepares app to transfer data and information to another device.
 */
public class TransferFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overschakelen, container, false);
        imageView = view.findViewById(R.id.barcode);
        Button thisDevice = view.findViewById(R.id.this_device);
        thisDevice.setOnClickListener(this);

        String userHash = new DBUser(getContext()).getUser().getKey();
        callDatabase(userHash);

        return view;
    }

    /**
     * @param userHash user key in hash form
     */
    private void callDatabase(final String userHash) {
        String tempKey = null;
        try {
            tempKey = Encryption.generateTempKey(userHash);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tempKey != null && userHash != null) {
            PrepareTransferParameter prepareTransferParameter = new PrepareTransferParameter(tempKey,
                    userHash);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<PrepareTransferResponse> call = apiService.prepareTransfer(prepareTransferParameter);
            final String finalTempKey = tempKey;
            call.enqueue(new Callback<PrepareTransferResponse>() {
                @Override
                public void onResponse(@NonNull Call<PrepareTransferResponse> call,
                                       @NonNull Response<PrepareTransferResponse> response) {
                    if (response.code() == 200) {
                        createBarcode(finalTempKey);
                    } else {
                        showPrepareTransferError(userHash);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrepareTransferResponse> call,
                                      @NonNull Throwable t) {
                    showPrepareTransferError(userHash);
                }
            });
        } else {
            showPrepareTransferError(userHash);
        }
    }

    /** Prepares retrieves user's data from database and prepares it in Json format.
     * @param userHash user key in hash form
     */
    private void showPrepareTransferError(final String userHash) {
        SnackbarHelper.showErrorSnackbar(
                getActivity().findViewById(android.R.id.content),
                R.string.wrong_tempkey_transfer, R.string.retry,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callDatabase(userHash);
                    }
                });
    }

    /** Creates a QR code to be read by a different device.
     * @param content String QR code content
     */
    private void createBarcode(String content) {
        BitMatrix result = null;
        try {
            result = new MultiFormatWriter().encode(content,
                    BarcodeFormat.QR_CODE, 512, 512, null);
        } catch (IllegalArgumentException | WriterException iae) {
            iae.printStackTrace();
        }

        if (result != null) {
            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, 512, 0, 0, w, h);

            imageView.setImageBitmap(bitmap);
        }
    }

    /** Catches click to begin transaction to a different fragment.
     * @param v view
     */
    @Override
    public void onClick(View v) {
        Fragment fragment = new CaptureFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment).commit();
    }
}
