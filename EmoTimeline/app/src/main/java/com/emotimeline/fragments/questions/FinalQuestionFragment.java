package com.emotimeline.fragments.questions;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.activities.DiaryEntryActivity;
import com.emotimeline.models.parameters.DiaryEntryAnswersParameter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Represents the final question of the diary entry survey
 *
 * @author Frank
 */
public class FinalQuestionFragment extends Fragment {

    @BindView(R.id.emoticon_group) RadioGroup emoticonGroup;
    @BindView(R.id.submit_entry_button) Button submitEntryButton;
    @OnClick(R.id.submit_entry_button) void submit() {submitEntry();}

    private int rating = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final_question, container, false);
        ButterKnife.bind(this, view);

        submitButtonEnabled(false);
        setupEmoticonGroup();

        return view;
    }

    /**
     * Hide previous and next button when fragment is visible to user.
     * @param isVisibleToUser true when fragment is visible
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() instanceof DiaryEntryActivity) {
                ((DiaryEntryActivity) getActivity()).hidePrevAndNextButton();
            }
        }
    }

    /**
     * Add OnCheckedChangeListener to RadioGroup with emoticons which changes the rating.
     */
    private void setupEmoticonGroup() {
        emoticonGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                submitButtonEnabled(true);
                switch (i) {
                    case R.id.first_emoticon:
                        rating = 1;
                        break;
                    case R.id.second_emoticon:
                        rating = 2;
                        break;
                    case R.id.third_emoticon:
                        rating = 3;
                        break;
                    case R.id.fourth_emoticon:
                        rating = 4;
                        break;
                    case R.id.fifth_emoticon:
                        rating = 5;
                        break;
                }
            }
        });
    }

    /**
     * Enable or disable submit button.
     * @param enabled enabled
     */
    private void submitButtonEnabled(boolean enabled) {
        if (enabled) {
            submitEntryButton.setAlpha(1f);
            submitEntryButton.setClickable(true);
        } else {
            submitEntryButton.setAlpha(.5f);
            submitEntryButton.setClickable(false);
        }
    }

    /**
     * Call activity method to insert data.
     */
    private void submitEntry() {
        Activity activity = getActivity();
        if (activity instanceof DiaryEntryActivity) {
            ((DiaryEntryActivity) activity).submitEntry(rating);
        }
    }
}
