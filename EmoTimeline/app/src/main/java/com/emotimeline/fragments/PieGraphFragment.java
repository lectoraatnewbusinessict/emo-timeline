package com.emotimeline.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.emotimeline.R;
import com.emotimeline.models.localdb.Emotion;
import com.emotimeline.localdb.DBMessage;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

/**
 * Creates and manages a pie_chart graph.
 */
public class PieGraphFragment extends Fragment {
    private PieChart pie_chart;
    View view;

    /**
     * @param savedInstanceState Bundle of data about instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * Creates and fills in the pie graph
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_piegraph, container, false);
        RelativeLayout graphLayout = view.findViewById(R.id.graph_layout);
        pie_chart = new PieChart(view.getContext());

        graphLayout.addView(pie_chart);
        configureGraphDisplay();
        configureGraphTouch();
        setGraphListener();

        addDataToGraph();

        customizeLegend();
        return view;
    }

    /**
     * Gets data for the pie_chart chart
     *
     * @return ArrayList<PieEntry>
     */
    private ArrayList<PieEntry> getPieEntries() {
        DBMessage emotionGrabber = new DBMessage(view.getContext());
        ArrayList<Emotion> emotions = emotionGrabber.getEmotions(2);
        ArrayList<PieEntry> pieEntries = new ArrayList<>();

        for (Emotion emotion : emotions) {
            /**Value 1 does not get loaded. This is a quick fix. Possibly caused by external library.*/
            int counterFix = emotion.getCount() * 10;

            pieEntries.add(new PieEntry(counterFix, emotion.getName()));
        }
        return pieEntries;
    }

    /**
     * Adds data to the pie_chart chart
     */
    private void addDataToGraph() {
        pie_chart.setData(createPieDataSet());
        pie_chart.highlightValues(null);
        pie_chart.invalidate();
    }

    /**
     * Creates pie_chart dataset.
     */
    private PieData createPieDataSet() {
        PieDataSet dataSet = new PieDataSet(getPieEntries(), "Emotie");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);
        dataSet.setColors(getColors());

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.BLACK);
        return data;
    }

    /**
     * creates a hole in the pie graph
     */
    private void createPieHole() {
        DisplayMetrics displayMetrics = getDisplayMetrics();
        pie_chart.setDrawHoleEnabled(true);
        pie_chart.setHoleColor(R.color.color_primary);
        pie_chart.setEntryLabelColor(Color.BLACK);
        pie_chart.setHoleRadius(7);
        pie_chart.setMinimumHeight(displayMetrics.heightPixels + 250);
        pie_chart.setMinimumWidth(displayMetrics.widthPixels);
        pie_chart.setTransparentCircleRadius(10);
    }

    /**
     * Returns various metrics about the graph display
     *
     * @return displayMetrics object
     */
    private DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displaymetrics);
        return displaymetrics;
    }

    /**
     * Returns a list of colors
     *
     * @return ArrayList<Integer> colors
     */
    private ArrayList<Integer> getColors() {
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());
        return colors;
    }


    /**
     * sets the graph's listener
     */
    private void setGraphListener() {
        pie_chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
            }

            @Override
            public void onNothingSelected() {
            }
        });
    }

    /**
     * Sets the description for the pier graph
     */
    private void setGraphDescription() {
        Description description = new Description();
        description.setText(getContext().getString(R.string.pie_graph_description));
        description.setTextSize(14);
        pie_chart.setDescription(description);
    }

    /**
     * configures the touch elements of the graph
     */
    private void configureGraphTouch() {
        pie_chart.setRotationEnabled(true);
        pie_chart.setRotationAngle(0);
    }

    /**
     * Sets various values related to the visual display of the graph.
     */
    private void configureGraphDisplay() {
        pie_chart.setUsePercentValues(true);
        pie_chart.setTop(20);
        pie_chart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        setGraphDescription();
        createPieHole();
    }

    /**
     * customizes the legend.
     */
    private void customizeLegend(){
        Legend legend = pie_chart.getLegend();
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        legend.setXEntrySpace(7);
        legend.setYEntrySpace(5);
    }
}
