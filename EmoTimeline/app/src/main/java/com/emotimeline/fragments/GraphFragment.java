package com.emotimeline.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emotimeline.R;
import com.emotimeline.adapters.GraphPagerAdapter;

/**
 * Manages the use of the Graph Adapter
 * Doesn't create any graphs itsself.
 */
public class GraphFragment extends Fragment {
    FragmentManager fm;
    ViewPager viewPager;
    GraphPagerAdapter graphPagerAdapter;

    public void setFragmentManager(FragmentManager fm) {
        this.fm = fm;
    }

    /**
     *
     * @param savedInstanceState Bundle of data about instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        graphPagerAdapter = new GraphPagerAdapter(fm);
    }

    /**
     * creates GraphPagerAdapter, calls super
     */
    @Override
    public void onResume() {
        super.onResume();
        graphPagerAdapter = new GraphPagerAdapter(fm);
    }

    /**
     *
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return View view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pager_graph, container, false);
        viewPager = view.findViewById(R.id.graph_pager);
        viewPager.setAdapter(graphPagerAdapter);
        return view;
    }
}
