package com.emotimeline.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.emotimeline.R;
import com.emotimeline.activities.SyncActivity;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.User;
import com.emotimeline.receivers.AlarmReceiver;
import com.emotimeline.services.NotificationService;

/**
 * Manages the settings page.
 */
public class SettingsFragment extends Fragment implements SyncActivity, View.OnFocusChangeListener, View.OnClickListener {

    View view;
    EditText nameEditField;
    AppCompatCheckBox globalNotificationsSwitch;
    TextView notificationTimeText;

    /**
     * @param savedInstanceState Bundle of data about instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /** creates view and fields
     * @param inflater           inflates fragment Layout
     * @param container          used in fragment inflation
     * @param savedInstanceState Bundle with data about the instance.
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        final LinearLayout linearLayout = view.findViewById(R.id.notification_text_layout);

        nameEditField = view.findViewById(R.id.naamveld);
        nameEditField.setOnFocusChangeListener(this);
        nameEditField.setText(new DBUser(getContext()).getUser().getName());
        setViewAndChildrenEnabled(linearLayout, receivesNotification());

        globalNotificationsSwitch = view.findViewById(R.id.all_notifications_toggle);
        globalNotificationsSwitch.setChecked(receivesNotification());
        globalNotificationsSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new DBUser(getContext()).getUser();
                new DBUser(getActivity()).updateNotificationSettings(getContext(), null,
                        !receivesNotification(), user.getNotificationHour(), user.getNotificationMinute());
                NotificationService.setReminder(getActivity(), AlarmReceiver.class,
                        user.getNotificationHour(), user.getNotificationMinute());
                globalNotificationsSwitch.setChecked(receivesNotification());
                setViewAndChildrenEnabled(linearLayout, receivesNotification());
            }
        });

        notificationTimeText = view.findViewById(R.id.settings_show_daily_time_picker);
        notificationTimeText.setText(getNotificationTimeString());
        setTextViewListener();

        return view;
    }

    /**
     * @param response String
     * @param responseType Class<?>
     */
    @Override
    public void performAfterSync(String response, Class<?> responseType) {
        /**TODO:Implement this performAfterSync */
        throw new IllegalStateException("Not implemented yet");
    }

    /**
     * handles focus change
     * @param v view
     * @param hasFocus boolean
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            Log.d("update user", nameEditField.getText().toString());
            DBUser dbUser = new DBUser(getContext());
            dbUser.updateName(dbUser.getUser(), nameEditField.getText().toString());
        }
    }

    /**
     * Handles button clicks
     * @param v clicked View
     */
    public void onClick(View v){
        if(v == notificationTimeText){
            showTimePickerDialog();
        }
    }

    /**
     * Check if user receives notifications
     * @return user receives notifications
     */
    private boolean receivesNotification() {
        User user = new DBUser(getContext()).getUser();
        return user.isReceivesNotification();
    }

    /**
     * Gives the formatted String for notification times.
     * @return the formatted String
     */
    private String getNotificationTimeString() {
        User user = new DBUser(getContext()).getUser();
        String notificationHour = String.valueOf(user.getNotificationHour());
        String notificationMinute = String.valueOf(user.getNotificationMinute());
        if (user.getNotificationHour() < 10) {
            notificationHour = "0" + notificationHour;
        }
        if (user.getNotificationMinute() < 10) {
            notificationMinute = "0" + notificationMinute;
        }
        return notificationHour + ":" + notificationMinute;
    }

    /**
     * Disable/enable a view and it's childs.
     * @param view the view
     * @param enabled disable or enable
     */
    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    /**
     * Sets the TextView listeners
     */
    private void setTextViewListener(){
        notificationTimeText = view.findViewById(R.id.settings_show_daily_time_picker);
        notificationTimeText.setOnClickListener(this);
    }

    /**
     * Displays a timePicker dialog and saves selected data.
     */
    public void showTimePickerDialog() {
        TimePickerDialog builder = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        new DBUser(getActivity()).updateNotificationSettings(getContext(), null,
                                receivesNotification(), hour, min);
                        NotificationService.setReminder(getActivity(), AlarmReceiver.class,
                                hour, min);
                        notificationTimeText.setText(getNotificationTimeString());
                    }
                }, new DBUser(getContext()).getUser().getNotificationHour(),
                new DBUser(getContext()).getUser().getNotificationMinute(), true);
        builder.show();
    }
}