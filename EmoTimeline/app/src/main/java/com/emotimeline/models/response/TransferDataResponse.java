package com.emotimeline.models.response;

import java.util.List;

/**
 * Represents the response from the transfer data call.
 *
 * @author Kevin
 */

public class TransferDataResponse {

    private boolean result;
    private List<TransferDataMessage> messages;
    private int id;
    private String hash;

    public TransferDataResponse(boolean result, List<TransferDataMessage> messages, int id, String hash) {
        this.result = result;
        this.messages = messages;
        this.id = id;
        this.hash = hash;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public List<TransferDataMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<TransferDataMessage> messages) {
        this.messages = messages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
