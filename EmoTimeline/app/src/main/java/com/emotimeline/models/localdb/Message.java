package com.emotimeline.models.localdb;

import com.google.gson.annotations.SerializedName;

/**
 * Message model used to locally save message.
 *
 * @author Kevin
 */
public class Message {

    public enum Sender {
        USER,
        COACH
    }

    @SerializedName("message_id")
    private int _id;

    @SerializedName(value="bericht", alternate={"message", "message2"})
    private String message;

    private transient Sender sender;

    @SerializedName("datum")
    private long date;

    private transient boolean isSynced;

    private String emotion;

    private String coach_reaction;

    @SerializedName("coach_behaviour")
    private String coach_behaviour;

    public Integer get_id()
    {
        return _id;
    }

    public void set_id(Integer _id)
    {
        this._id = _id;
    }

    public Message(int messageId, String emotion, long date, String message, String coach_behaviour) {
        this._id = messageId;
        this.emotion = emotion;
        this.date = date;
        this.message = message;
        this.coach_behaviour = coach_behaviour;
    }

    public Message(String message, Sender sender) {
        this();
        this.message = message;
        this.sender = sender;
    }

    public Message() {
        date = System.currentTimeMillis() / 1000;
        isSynced = false;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public String getCoachReaction()
    {
        return coach_reaction;
    }

    public void setCoachReaction(String coach_reaction)
    {
        this.coach_reaction = coach_reaction;
    }

    public String getEmotion()
    {
        return emotion;
    }

    public void setEmotion(String emotion)
    {
        this.emotion = emotion;
    }

    public String getCoachBehaviour()
    {
        return coach_behaviour;
    }

    public void setCoachBehaviour(String coachBehaviour)
    {
        this.coach_behaviour = coachBehaviour;
    }

}
