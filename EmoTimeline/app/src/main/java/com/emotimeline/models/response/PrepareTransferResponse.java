package com.emotimeline.models.response;

/**
 * Represents the response from the prepare transfer call.
 *
 * @author Kevin
 */

public class PrepareTransferResponse {

    private boolean result;
    private String message;

    public PrepareTransferResponse() {}
}
