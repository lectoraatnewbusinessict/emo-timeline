package com.emotimeline.models.parameters;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the model to save a given answer using the question id.
 *
 * @author Kevin
 */
public class DiaryEntryAnswersParameter {

    private String text;
    @SerializedName("question_id")
    private int questionId;

    public DiaryEntryAnswersParameter(String text, int questionId) {
        this.text = text;
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
