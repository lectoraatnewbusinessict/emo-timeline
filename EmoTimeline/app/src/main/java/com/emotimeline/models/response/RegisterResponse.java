package com.emotimeline.models.response;

/**
 * Represents the response from the register call
 *
 * @author Kevin
 */

public class RegisterResponse {

    private boolean result;
    private String key;
    private int id;
    private String message;

    public RegisterResponse(boolean result, String key, int id, String message) {
        this.result = result;
        this.key = key;
        this.id = id;
        this.message = message;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
