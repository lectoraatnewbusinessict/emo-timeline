package com.emotimeline.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Represents a Question in the Diary. Contains Answers
 *
 * @author Frank
 */
public class QuestionModel {
    private int id;
    private String text;
    @SerializedName("has_other")
    private Boolean hasOther;
    @SerializedName("is_skippable")
    private Boolean isSkippable;
    private int sequence;
    private ArrayList<AnswerModel> answers;
    @SerializedName("step_id")
    private int stepId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getHasOther() {
        return hasOther;
    }

    public void setHasOther(Boolean hasOther) {
        this.hasOther = hasOther;
    }

    public Boolean getSkippable() {
        return isSkippable;
    }

    public void setSkippable(Boolean skippable) {
        isSkippable = skippable;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public ArrayList<AnswerModel> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerModel> answers) {
        this.answers = answers;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }
}
