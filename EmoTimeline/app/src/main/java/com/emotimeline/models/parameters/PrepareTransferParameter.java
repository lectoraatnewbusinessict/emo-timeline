package com.emotimeline.models.parameters;

/**
 * Represents the parameters for the prepare transfer call.
 *
 * @author Kevin
 */

public class PrepareTransferParameter {

    private String tempKey;
    private String hash;

    public PrepareTransferParameter(String tempKey, String hash) {
        this.tempKey = tempKey;
        this.hash = hash;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
