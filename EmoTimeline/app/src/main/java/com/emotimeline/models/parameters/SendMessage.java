package com.emotimeline.models.parameters;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the message in the send message parameter model.
 *
 * @author Kevin
 */

public class SendMessage {

    @SerializedName("message_id")
    private int id;
    @SerializedName("datum")
    private long date;
    @SerializedName("bericht")
    private String message;
    @SerializedName("coach_behaviour")
    private String coachBehaviour;

    public SendMessage(int id, long date, String message, String coachBehaviour) {
        this.id = id;
        this.date = date;
        this.message = message;
        this.coachBehaviour = coachBehaviour;
    }
}
