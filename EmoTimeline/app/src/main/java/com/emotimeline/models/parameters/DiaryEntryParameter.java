package com.emotimeline.models.parameters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Represents the model send to the API, which inserts a new diary entry in the database.
 *
 * @author Kevin
 */
public class DiaryEntryParameter {

    private int rating;
    private List<DiaryEntryAnswersParameter> answers;
    @SerializedName("user_id")
    private int userId;

    public DiaryEntryParameter(int rating, List<DiaryEntryAnswersParameter> answers, int userId) {
        this.rating = rating;
        this.answers = answers;
        this.userId = userId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<DiaryEntryAnswersParameter> getAnswers() {
        return answers;
    }

    public void setAnswers(List<DiaryEntryAnswersParameter> answers) {
        this.answers = answers;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
