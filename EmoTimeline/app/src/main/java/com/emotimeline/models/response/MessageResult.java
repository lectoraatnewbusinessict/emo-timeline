package com.emotimeline.models.response;

import java.util.List;

/**
 * Represents the results in the message response model.
 *
 * @author Kevin
 */

public class MessageResult {

    private int id;
    private List<String> emotions;
    private String response;

    public MessageResult(int id, List<String> emotions, String response) {
        this.id = id;
        this.emotions = emotions;
        this.response = response;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getEmotions() {
        return emotions;
    }

    public void setEmotions(List<String> emotions) {
        this.emotions = emotions;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
