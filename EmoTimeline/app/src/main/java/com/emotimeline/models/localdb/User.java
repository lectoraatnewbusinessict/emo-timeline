package com.emotimeline.models.localdb;

import android.graphics.Bitmap;

import com.emotimeline.utils.ImageConverter;
import com.google.gson.annotations.SerializedName;

/**
 * User model used to locally save user.
 *
 * @author Kevin
 */
public class User
{
    @SerializedName("id")
    private int _id;
    private String key;
    private transient byte[] coachImage;
    private transient String coachName;
    private transient String name;
    private String coach_behaviour;
    private transient boolean receivesNotification;
    private int notificationHour;
    private int notificationMinute;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public byte[] getCoachImage()
    {
        return coachImage;
    }

    public void setCoachImage(byte[] coachImage)
    {
        this.coachImage = coachImage;
    }

    public String getCoachName()
    {
        return coachName;
    }

    public void setCoachName(String coachName)
    {
        this.coachName = coachName;
    }

    public void setCoachImage(Bitmap coachImage)
    {
        this.coachImage = ImageConverter.bitmapToBytes(coachImage);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCoach_behaviour()
    {
        return coach_behaviour;
    }

    public void setCoach_behaviour(String coach_behaviour)
    {
        this.coach_behaviour = coach_behaviour;
    }

    public boolean isReceivesNotification() {
        return receivesNotification;
    }

    public void setReceivesNotification(boolean receivesNotification) {
        this.receivesNotification = receivesNotification;
    }

    public int getNotificationHour() {
        return notificationHour;
    }

    public void setNotificationHour(int notificationHour) {
        this.notificationHour = notificationHour;
    }

    public int getNotificationMinute() {
        return notificationMinute;
    }

    public void setNotificationMinute(int notificationMinute) {
        this.notificationMinute = notificationMinute;
    }
}
