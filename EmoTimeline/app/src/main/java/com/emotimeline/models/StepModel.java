package com.emotimeline.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Represents a Step in the Diary. Containing Questions and Answers
 *
 * @author Frank
 */

public class StepModel {
    private int id;
    private String title;
    private String tag;
    @SerializedName("next_step")
    private int nextStep;
    private ArrayList<QuestionModel> questions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getNextStep() {
        return nextStep;
    }

    public void setNextStep(int nextStep) {
        this.nextStep = nextStep;
    }

    public ArrayList<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionModel> questions) {
        this.questions = questions;
    }
}
