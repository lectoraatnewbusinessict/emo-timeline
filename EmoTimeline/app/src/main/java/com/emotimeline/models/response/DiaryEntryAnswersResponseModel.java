package com.emotimeline.models.response;

/**
 * Represents the model received from the getDiaryEntryAnswers API call.
 *
 * @author Kevin
 */
public class DiaryEntryAnswersResponseModel {

    private String question;
    private String answer;

    public DiaryEntryAnswersResponseModel(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
