package com.emotimeline.models;

import android.support.annotation.Nullable;

/**
 * Represent the answer given by the user.
 *
 * @author Kevin
 */
public class GivenAnswerModel {

    private int stepId;
    private int questionId;
    private int[] answerIds;
    private String hasOtherText;
    private int sliderValue;

    public GivenAnswerModel(int stepId, int questionId, @Nullable int[] answerIds, @Nullable String hasOtherText) {
        this.stepId = stepId;
        this.questionId = questionId;
        this.answerIds = answerIds;
        this.hasOtherText = hasOtherText;
        this.sliderValue = -1;
    }

    public GivenAnswerModel(int stepId, int questionId, int sliderValue) {
        this.stepId = stepId;
        this.questionId = questionId;
        this.sliderValue = sliderValue;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int[] getAnswerIds() {
        return answerIds;
    }

    public void setAnswerIds(int[] answerIds) {
        this.answerIds = answerIds;
    }

    public String getHasOtherText() {
        return hasOtherText;
    }

    public void setHasOtherText(String hasOtherText) {
        this.hasOtherText = hasOtherText;
    }

    public int getSliderValue() {
        return sliderValue;
    }
}
