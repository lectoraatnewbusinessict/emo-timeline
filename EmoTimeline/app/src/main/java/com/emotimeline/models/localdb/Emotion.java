package com.emotimeline.models.localdb;

/**
 * Emotion model used to locally save emotion.
 *
 * @author Kevin
 */

public class Emotion {
    private String name;
    private int count;
    private String date;

    public Emotion(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public Emotion(String name, int count, String date) {
        this(name, count);
        this.setDate(date);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
