package com.emotimeline.models.parameters;

/**
 * Represents the parameters for the data transfer call.
 *
 * @author Kevin
 */

public class TransferDataParameter {

    private String tempKey;

    public TransferDataParameter(String tempKey) {
        this.tempKey = tempKey;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }
}
