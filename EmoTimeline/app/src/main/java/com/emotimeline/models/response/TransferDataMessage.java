package com.emotimeline.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the messages in the transfer data response model.
 *
 * @author Kevin
 */

public class TransferDataMessage {

    private int id;
    @SerializedName("message_id")
    private int messageId;
    private String emotion;
    @SerializedName("datum")
    private int date;
    private String message;
    private String hash;
    @SerializedName("coach_behaviour")
    private String coachBehaviour;

    public TransferDataMessage(int id, int messageId, String emotion, int date, String message, String hash, String coachBehaviour) {
        this.id = id;
        this.messageId = messageId;
        this.emotion = emotion;
        this.date = date;
        this.message = message;
        this.hash = hash;
        this.coachBehaviour = coachBehaviour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getCoachBehaviour() {
        return coachBehaviour;
    }

    public void setCoachBehaviour(String coachBehaviour) {
        this.coachBehaviour = coachBehaviour;
    }
}
