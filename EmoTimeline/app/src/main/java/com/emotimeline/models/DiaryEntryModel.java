package com.emotimeline.models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Represents a Diary Entry
 *
 * @author Peter
 */
public class DiaryEntryModel {

    private int id;
    @SerializedName("rating")
    private int mRatingId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    /**
     * Converts a timestamp written in seconds to a day month year format
     * @param date a date
     * @return String Date time in dutch locale (22 april 1994 00:00)
     */
    private String convertToDate(String date){
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat requiredFormat = new SimpleDateFormat("d MMMM YYYY H:mm", Locale.forLanguageTag("nl_NL"));

        String reformattedStr = "";
        try {
            reformattedStr = requiredFormat.format(originalFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return reformattedStr;
    }
    /**
     * Gets the RatingId, which represents a rating of the diary entry. This can be an integer from
     * 1 to 5.
     * @return mRatingId
     */
    public int getRatingId(){
        return mRatingId;
    }

    /**
     * Gets the Ratingid, which represents a rating of the diary entry.
     * @param  ratingId This can be positive (1), negative (-1) or neutral (0)
     *
    */
     public void setRatingId(int ratingId){
            mRatingId = ratingId;
     }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFormattedCreatedAt() {
        return convertToDate(createdAt);
    }

    public String getFormattedUpdatedAt() {
        return convertToDate(updatedAt);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
