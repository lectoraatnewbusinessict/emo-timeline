package com.emotimeline.models.parameters;

import java.util.List;

/**
 * Represents the parameters for the send message call.
 *
 * @author Kevin
 */

public class SendMessageParameter {

    private List<SendMessage> messages;
    private String hash;

    public SendMessageParameter(List<SendMessage> messages, String hash) {
        this.messages = messages;
        this.hash = hash;
    }

    public List<SendMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<SendMessage> messages) {
        this.messages = messages;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
