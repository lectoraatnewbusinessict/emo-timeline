package com.emotimeline.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Represents the response from the send message call.
 *
 * @author Kevin
 */

public class MessageResponse {

    @SerializedName("message_results")
    private List<MessageResult> messageResults;
    private boolean result;

    public MessageResponse(List<MessageResult> messageResults, boolean result) {
        this.messageResults = messageResults;
        this.result = result;
    }

    public List<MessageResult> getMessageResults() {
        return messageResults;
    }

    public void setMessageResults(List<MessageResult> messageResults) {
        this.messageResults = messageResults;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
