package com.emotimeline.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emotimeline.services.RestartAlarmService;

/**
 * This receiver is called when the device reboots.
 *
 * @author Kevin
 */
public class BootCompletedReceiver extends BroadcastReceiver {

    /**
     * This method is called when the device reboots. It then calls the RestartAlarmService.
     *
     * @param context the context which calls the method
     * @param intent the intent send by the device reboot
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent i = new Intent(context, RestartAlarmService.class);
            context.startService(i);
        }
    }
}