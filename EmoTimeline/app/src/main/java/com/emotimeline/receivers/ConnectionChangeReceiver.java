package com.emotimeline.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.emotimeline.models.localdb.Message;
import com.emotimeline.localdb.DBMessage;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.parameters.SendMessage;
import com.emotimeline.models.parameters.SendMessageParameter;
import com.emotimeline.models.response.MessageResponse;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;
import com.emotimeline.services.MessageService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Used to send messages to the API which have not been send yet.
 *
 * @author Kevin
 */
public class ConnectionChangeReceiver extends BroadcastReceiver {

    /**
     * Checks if app has internet access.
     *
     * @param context context in which method is called
     * @return whether the app has internet access or not
     */
    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * Called when connection is changed. If app has internet access, sync not synced messages.
     *
     * @param context context in which method is called
     * @param intent intent from which method is called
     */
    @Override
    public void onReceive(final Context context, final Intent intent) {

        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction()) ||
                "android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
            if (isOnline(context)) {
                DBMessage dbMessage = new DBMessage(context);

                ArrayList<Message> notSynced = dbMessage.getNotSyncedMessages();
                List<SendMessage> notSyncedParameters = new ArrayList<>();
                for (Message message : notSynced) {
                    SendMessage messageParameter = new SendMessage(message.get_id(), message.getDate(),
                            message.getMessage(), message.getCoachBehaviour());
                    notSyncedParameters.add(messageParameter);
                }

                if (notSyncedParameters.size() > 0) {
                    sendMessages(context, notSyncedParameters);
                }
            }
        }
    }

    /**
     * Send messages to API.
     *
     * @param context context in which method is called
     * @param messages messages that need to be send
     */
    private void sendMessages(final Context context, List<SendMessage> messages) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        SendMessageParameter sendMessageParameter = new SendMessageParameter(
                messages, new DBUser(context).getUser().getKey());
        Call<MessageResponse> call = apiService.sendMessage(sendMessageParameter);
        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(@NonNull Call<MessageResponse> call,
                                   @NonNull Response<MessageResponse> response) {
                if (response.code() == 200) {
                    MessageService.syncMessages(context, response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MessageResponse> call, @NonNull Throwable t) {
            }
        });
    }
}
