package com.emotimeline.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emotimeline.R;
import com.emotimeline.activities.DiaryEntryActivity;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.User;
import com.emotimeline.services.NotificationService;

/**
 * This class represents the receiver called when a alarm is sent. When the alarm is received the
 * notification will be shown.
 *
 * @author Kevin
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        User user = new DBUser(context).getUser();
        if (user.isReceivesNotification()) {
            NotificationService.showNotification(context, DiaryEntryActivity.class,
                    context.getString(R.string.notification_title), "");
        }
    }
}