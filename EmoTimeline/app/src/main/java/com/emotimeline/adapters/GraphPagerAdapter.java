package com.emotimeline.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.emotimeline.fragments.LineGraphFragment;
import com.emotimeline.fragments.PieGraphFragment;

/**
 * Manages the pages of the graphs.
 * @author peter
 */
public class GraphPagerAdapter extends FragmentStatePagerAdapter {
    private String[] titles = { "Emoties totaal", "Emoties per dag" };

    public GraphPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /**
     * Gets a fragment of type based on position
     * @param position The position of the page in the list starting with 0
     * @return fragment
     */
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PieGraphFragment();
        }
        else {
            return new LineGraphFragment();
        }
    }

    /**
     * Returns the number of pages
     * @return Number of pages
     */
    @Override
    public int getCount() {
        return 2;
    }

    /**
     * Gets the page title based on the position
     * @param position The position of the page in the list starting with 0
     * @return the page title
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
