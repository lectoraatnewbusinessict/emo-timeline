package com.emotimeline.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emotimeline.R;
import com.emotimeline.models.localdb.Message;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * This class provides a binding between the messages data and the messages Recyclerview
 * @author peter
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private ArrayList<Message> messages;
    private final static int FIRST_MESSAGE = 99;

    static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout messageView;
        ViewHolder(LinearLayout v) {
            super(v);
            messageView = v;
        }
    }

    public MessageAdapter(ArrayList<Message> messages) {
        this.messages = messages;
    }

    /**
     * Add layout based on viewtype for the messages with an extra margin for the first message
     * @param parent the parent Viewgroup
     * @param viewType The Viewtyppe is either a Coach or a user
     * @return Viewholder with the messagelayout
     */
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        if (viewType == Message.Sender.COACH.ordinal()
         || Message.Sender.COACH.ordinal() == viewType  - FIRST_MESSAGE) {
            layout = R.layout.message_view_coach;
        }
        else {
            layout = R.layout.message_view_user;
        }

        LinearLayout messageLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        if (viewType >= FIRST_MESSAGE) {
            ((ViewGroup.MarginLayoutParams) messageLayout.getLayoutParams()).topMargin =
                    (int) (50 * parent.getContext().getResources().getDisplayMetrics().density);
        }
        return new ViewHolder(messageLayout);
    }

    /**
     * Adds the message text, timestamp and a checkmark, and binds this to the view
     * @param holder The Current viewholder to bind the data to
     * @param position The position of the message
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = messages.get(position);

        ((TextView)((LinearLayout)holder.messageView.getChildAt(0)).getChildAt(0))
                .setText(message.getMessage());

        LinearLayout footer = ((LinearLayout)(holder.messageView.getChildAt(1)));

        String formattedDate = getFormattedDate(message.getDate(), holder.messageView.getContext());
        ((TextView)footer.getChildAt(0)).setText(formattedDate);

        if (message.getSender() == Message.Sender.USER)
            footer.getChildAt(1).setVisibility(message.isSynced() ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * Returns the formatted date to dutch locale
     * @param date The date in millisecconds
     * @param context The current Context
     * @return
     */
    private String getFormattedDate(long date, Context context) {
        long dateMilliSeconds = date * 1000;

        if(!DateUtils.isToday(dateMilliSeconds))
            return new SimpleDateFormat("d MMMM H:mm", Locale.forLanguageTag("nl_NL")).format(new Date(dateMilliSeconds));

        long timeDiff = System.currentTimeMillis() - dateMilliSeconds;
        if (timeDiff < 60 * 60 * 1000)
            return new SimpleDateFormat("m", Locale.forLanguageTag("nl_NL")).format(new Date(timeDiff))
                    + context.getResources().getString(R.string.messageTimeMinutesAgo);
        else
            return new SimpleDateFormat("H", Locale.forLanguageTag("nl_NL")).format(new Date(timeDiff))
                    + context.getResources().getString(R.string.messageTimeHoursAgo);
    }

    /**
     * Returns the view type based on position
     * @param position the position of the message
     * @return the type of the message
     */
    @Override
    public int getItemViewType(int position) {
        int type = messages.get(position).getSender().ordinal();
        if (position == 0){
            return FIRST_MESSAGE + type;
        }
        return type;
    }

    /**
     * Returns the number of messages
     * @return number of messages
     */
    @Override
    public int getItemCount() {
        return messages.size();
    }
}
