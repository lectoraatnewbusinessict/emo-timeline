package com.emotimeline.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.activities.DiaryEntryDetailActivity;
import com.emotimeline.helpers.EmojiHelper;
import com.emotimeline.models.DiaryEntryModel;

import java.util.List;

/**
 * Adapter between DiaryEntryModel and row layout in the RecyclerView.
 *
 * @author Peter
 */
public class DiaryOverviewAdapter extends RecyclerView.Adapter<DiaryOverviewAdapter.DiaryOverviewHolder> {

    private List<DiaryEntryModel> entries;

    public DiaryOverviewAdapter(List<DiaryEntryModel> entries){
        this.entries = entries;
    }

    @Override
    public DiaryOverviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_diary_entry, parent, false);
        return new DiaryOverviewHolder(view);
    }

    @Override
    public void onBindViewHolder(DiaryOverviewHolder holder, int position) {
        final DiaryEntryModel entry = entries.get(position);
        holder.icon.setImageResource(EmojiHelper.getEmoticon(entry.getRatingId()));
        holder.date.setText(entry.getFormattedCreatedAt());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppController.getInstance().setCurrentDiaryEntry(entry);
                Intent intent = new Intent(view.getContext(), DiaryEntryDetailActivity.class);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (entries == null) {
            return 0;
        }
        return entries.size();
    }

    /**
     * Custom ViewHolder to hold the views from the row layout.
     */
    static class DiaryOverviewHolder extends RecyclerView.ViewHolder{
        CardView card;
        ImageView icon;
        TextView date;

        DiaryOverviewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.diary_entry_card);
            icon = itemView.findViewById(R.id.diary_entries_rating);
            date = itemView.findViewById(R.id.diary_entries_textview_date);
        }
    }
}
