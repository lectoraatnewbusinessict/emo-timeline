package com.emotimeline.network;

import com.emotimeline.models.DiaryEntryModel;
import com.emotimeline.models.StepModel;
import com.emotimeline.models.parameters.*;
import com.emotimeline.models.response.*;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * This class provides methods for all api calls needed in this app. The annotation from Retrofit
 * shows which type of call is to be made.
 *
 * @author Kevin
 */
public interface ApiInterface {

    /**
     * Represents the GET call to register a new user.
     *
     * @return the response model
     */
    @GET("api/v1/register/")
    Call<RegisterResponse> register();

    /**
     * Represents the POST call to send a message.
     *
     * @return the response model
     */
    @POST("api/v1/sendMessage/")
    Call<MessageResponse> sendMessage(@Body SendMessageParameter sendMessageParameter);

    /**
     * Represents the POST call to prepare transfer.
     *
     * @return the response model
     */
    @POST("api/v1/prepareTransfer/")
    Call<PrepareTransferResponse> prepareTransfer(@Body PrepareTransferParameter prepareTransferParameter);

    /**
     * Represents the POST call to data transfer.
     *
     * @return the response model
     */
    @POST("api/v1/transferData/")
    Call<TransferDataResponse> transferData(@Body TransferDataParameter transferDataParameter);

    /**
     * Represents the GET call to fetch the diary entries of a user
     * User id is send as a hash in header.
     * @return the response model
     */
    @GET("api/v1/getDiaryEntries/")
    Call<List<DiaryEntryModel>> getDiaryEntries(@Header("hash")String userhash);

    /**
     * Represents the GET call to fetch the questions from the database
     * @return the response model with Steps, Questions & Answers
     */
    @GET("api/v1/getQuestions")
    Call<List<StepModel>> getQuestions();

    /**
     * Represents the GET call to fetch the diary answers from the database
     * @return the response model
     */
    @GET("api/v1/getDiaryEntryAnswers/")
    Call<List<DiaryEntryAnswersResponseModel>> getDiaryAnswers(@Header("hash")String userhash, @Header("entry_id")int entryId);

    /** Represents the POST call to insert a new diary entry in the database
     * @return default response body
     */
    @POST("api/v1/entries")
    Call<ResponseBody> entries(@Header("hash")String userhash, @Body DiaryEntryParameter diaryEntryParameter);
}
