package com.emotimeline.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.helpers.EmojiHelper;
import com.emotimeline.helpers.SnackbarHelper;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.DiaryEntryModel;
import com.emotimeline.models.response.DiaryEntryAnswersResponseModel;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Show the given answers for certain questions within an entry.
 *
 * @author Kevin
 */
public class DiaryEntryDetailActivity extends AppCompatActivity {

    @BindView(R.id.diary_entry_toolbar) Toolbar toolbar;
    @BindView(R.id.diary_entry_header) ImageView headerImageView;
    @BindView(R.id.diary_entry_date_card) CardView cardView;
    @BindView(R.id.diary_entry_date) TextView dateTextView;
    @BindView(R.id.diary_entry_emoticon) ImageView emoticonImageView;
    @BindView(R.id.diary_entry_detail_progress) ProgressBar progressBar;
    @BindView(R.id.diary_entry_list) LinearLayout listView;

    private DiaryEntryModel entryModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_entry_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary_dark));

        setupHeader();

        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.color_accent),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        entryModel = AppController.getInstance().getCurrentDiaryEntry();
        dateTextView.setText(entryModel.getFormattedCreatedAt());
        emoticonImageView.setImageResource(EmojiHelper.getWhiteEmoticon(entryModel.getRatingId()));

        getDiaryAnswers();
    }

    /**
     * Calls the onBackPressed method to go back in the navigation stack
     * @return boolean
     */
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    /**
     * Do the API call to get the diary answers.
     */
    private void getDiaryAnswers() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<DiaryEntryAnswersResponseModel>> call = apiService.getDiaryAnswers(new DBUser(this).getUser().getKey(), entryModel.getId());
        call.enqueue(new Callback<List<DiaryEntryAnswersResponseModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<DiaryEntryAnswersResponseModel>> call,
                                   @NonNull Response<List<DiaryEntryAnswersResponseModel>> response) {
                if (response.code() == 200) {
                    List<DiaryEntryAnswersResponseModel> res = response.body();
                    if (res != null) {
                        for (DiaryEntryAnswersResponseModel d : res) {
                            addLayout(d.getQuestion(), d.getAnswer());
                        }
                        progressBar.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                    }
                } else {
                    setFailedMessage();
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<DiaryEntryAnswersResponseModel>> call, @NonNull Throwable t) {
                setFailedMessage();
            }
        });
    }

    /**
     * When the header image has a height, set the top margin of the emoticon.
     */
    private void setupHeader() {
        headerImageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)emoticonImageView.getLayoutParams();
                Resources r = DiaryEntryDetailActivity.this.getResources();
                int px = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        50,
                        r.getDisplayMetrics()
                );
                params.setMargins(0, headerImageView.getHeight()-px, 0, 0);
                emoticonImageView.setLayoutParams(params);
                headerImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    /**
     * Add a layout with the corresponding text.
     * @param questionText the question
     * @param answerText the answer
     */
    private void addLayout(String questionText, String answerText) {
        View listLayout = LayoutInflater.from(this).inflate(R.layout.list_item_given_answer, listView, false);

        TextView questionTextView = listLayout.findViewById(R.id.diary_entry_question);
        TextView answerTextView = listLayout.findViewById(R.id.diary_entry_answer);

        questionTextView.setText(questionText);
        answerTextView.setText(answerText);

        listView.addView(listLayout);
    }

    /**
     * Set a failed message in the snackbar, when user clicks try again try to get data again.
     */
    private void setFailedMessage() {
        SnackbarHelper.showErrorSnackbar(findViewById(android.R.id.content), R.string.prompt_diary_entries_answers_failed, R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDiaryAnswers();
            }
        });
    }
}
