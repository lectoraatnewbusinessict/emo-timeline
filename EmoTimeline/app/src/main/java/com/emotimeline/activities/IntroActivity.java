package com.emotimeline.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.emotimeline.R;
import com.emotimeline.fragments.slides.PrivacyFragment;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * This class handles the first screen of the App. It is initialized when the app is started for the very first time
 * The slides are giving instructions on how to use the app
 *
 * @Author Frank
 */

public class IntroActivity extends AppIntro {

    PrivacyFragment privacy = new PrivacyFragment();

    /**
     * Default onCreate method Android
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlides();
        setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.color_primary));
        setSeparatorColor(Color.parseColor("#2196F3"));
        showSkipButton(false);
        setProgressButtonEnabled(true);
        setFlowAnimation();
        setVibrate(false);
        setVibrateIntensity(30);
    }

    /**
     * Skips the tutorial and finishes the AppIntro
     * @param currentFragment
     */
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    /**
     * Dismisses the AppIntro after all slides are shown
     * @param currentFragment
     */
    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }

    /**
     * This method switches between different fragments of the AppIntro slides
     * @param oldFragment
     * @param newFragment
     */
    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }

    /**
     * Add slides to the AppIntro inherited class
     */
    private void addSlides(){
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dia_1_kop),
                getString(R.string.intro_dia_1_beschrijving), R.drawable.denkjezelf,
                ContextCompat.getColor(getApplicationContext(), R.color.color_white)));
        addSlide(privacy);
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dia_2_kop),
                getString(R.string.intro_dia_2_beschrijving), R.drawable.coach,
                Color.parseColor("#42A5F5")));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dia_3_kop),
                getString(R.string.intro_dia_3_beschrijving), R.drawable.slide3,
                ContextCompat.getColor(getApplicationContext(), R.color.color_accent)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dia_4_kop),
                getString(R.string.intro_dia_4_beschrijving), R.drawable.slide4,
                Color.parseColor("#FFEE58")));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dia_5_kop),
                getString(R.string.intro_dia_5_beschrijving), R.drawable.slide5,
                Color.parseColor("#FFA726")));
    }
}