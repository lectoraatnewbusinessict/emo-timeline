package com.emotimeline.activities;

/**
 * Created by Jordy on 13-10-2016.
 */
public interface SyncActivity
{
    void performAfterSync(String response, Class<?> responseType);
}
