package com.emotimeline.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.emotimeline.AppController;
import com.emotimeline.R;
import com.emotimeline.fragments.questions.CheckboxQuestionFragment;
import com.emotimeline.fragments.questions.FinalQuestionFragment;
import com.emotimeline.fragments.questions.QuestionFragment;
import com.emotimeline.fragments.questions.RadioQuestionFragment;
import com.emotimeline.fragments.questions.SliderQuestionFragment;
import com.emotimeline.fragments.questions.TextQuestionFragment;
import com.emotimeline.helpers.SnackbarHelper;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.AnswerModel;
import com.emotimeline.models.GivenAnswerModel;
import com.emotimeline.models.QuestionModel;
import com.emotimeline.models.StepModel;
import com.emotimeline.models.parameters.DiaryEntryAnswersParameter;
import com.emotimeline.models.parameters.DiaryEntryParameter;
import com.emotimeline.network.ApiClient;
import com.emotimeline.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Represents the question lists for Diary entries
 * @author Frank
 */
public class DiaryEntryActivity extends AppCompatActivity {

    private QuestionsPagerAdapter questionPagerAdapter;
    private List<StepModel> steps;
    private List<QuestionModel> questions = new ArrayList<>();
    private Stack<Integer> questionsStack = new Stack<>();

    @BindView(R.id.diary_entry_progress) ProgressBar progressBar;
    @BindView(R.id.prev_next_buttons) RelativeLayout relativeLayout;
    @BindView(R.id.question_view_pager) QuestionViewPager questionViewPager;
    @BindView(R.id.prev_button) Button prevButton;
    @BindView(R.id.next_button) Button nextButton;

    @OnClick(R.id.prev_button) void previousQuestion() { previousButtonClicked(); }
    @OnClick(R.id.next_button) void nextQuestion() { nextButtonClicked(); }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diaryentry);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(
                    ContextCompat.getColor(this.getApplicationContext(), R.color.color_primary)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.color_accent),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        getQuestions();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() { super.onPause();
    }

    /**
     * Calls the onBackPressed method to go back in the navigation stack
     * @return boolean
     */
    @Override
    public boolean onSupportNavigateUp(){
        if (AppController.getInstance().getGivenAnswers().size() > 0) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.diary_exit))
                    .setMessage(getString(R.string.diary_exit_question))
                    .setPositiveButton(getString(R.string.diary_exit_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AppController.getInstance().clearGivenAnswers();
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.diary_exit_no), null)
                    .show();
            return false;
        } else {
            onBackPressed();
            return true;
        }
    }

    /**
     * Hide the previous and next button bar.
     */
    public void hidePrevAndNextButton() {
        relativeLayout.setVisibility(View.GONE);
    }

    /**
     * Sort the given answers and do the API call.
     */
    public void submitEntry(int rating) {
        List<DiaryEntryAnswersParameter> givenAnswers = new ArrayList<>();
        for (GivenAnswerModel answer:AppController.getInstance().getGivenAnswers()) {
            StringBuilder formattedGivenAnswer = new StringBuilder();
            if (answer.getAnswerIds() != null) {
                int count = 0;
                for (int id : answer.getAnswerIds()) {
                    AnswerModel answerModel = getAnswer(answer.getStepId(), answer.getQuestionId(), id);
                    if (answerModel != null) {
                        if (count != 0) {
                            formattedGivenAnswer.append(", ");
                        }
                        formattedGivenAnswer.append(answerModel.getText());
                        count++;
                    }
                }
            }

            String otherText = answer.getHasOtherText();
            if (otherText != null && !TextUtils.isEmpty(otherText)) {
                formattedGivenAnswer.append(otherText);
            }

            int sliderValue = answer.getSliderValue();
            if (sliderValue != -1) {
                formattedGivenAnswer.append(sliderValue);
            }

            givenAnswers.add(new DiaryEntryAnswersParameter(formattedGivenAnswer.toString(), answer.getQuestionId()));
        }

        if (givenAnswers.size() > 0) {
            DiaryEntryParameter diaryEntryParameter = new DiaryEntryParameter(rating, givenAnswers,
                    new DBUser(getApplicationContext()).getUser().get_id());
            insertDiaryEntry(diaryEntryParameter);
        }
    }

    /**
     * Setup view pager and connected views.
     */
    private void initViewPager() {
        questionViewPager.setAdapter(questionPagerAdapter);
        questionViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (questionsStack.isEmpty()) {
                    prevButton.setEnabled(false);
                } else {
                    prevButton.setEnabled(true);
                }
            }
        });
        progressBar.setVisibility(View.GONE);
        questionViewPager.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        prevButton.setEnabled(false);
        nextButton.setEnabled(true);
    }

    /**
     * Represents the API call to get the questions.
     */
    private void getQuestions() {
        Call<List<StepModel>> call = ApiClient.getClient().create(ApiInterface.class).getQuestions();
        call.enqueue(new Callback<List<StepModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<StepModel>> call,
                                   @NonNull Response<List<StepModel>> response) {
                if (response.code() == 200) {
                    steps = response.body();
                    setQuestions(steps);
                    AppController.getInstance().setSteps(steps);
                    questionPagerAdapter = new QuestionsPagerAdapter(getSupportFragmentManager());
                    initViewPager();
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<StepModel>> call, @NonNull Throwable t) {
            }
        });
    }

    /**
     * Create a single array for all the questions.
     */
    private void setQuestions(List<StepModel> steps) {
        for (StepModel step:steps) {
            questions.addAll(step.getQuestions());
        }
    }

    /**
     * Determine which fragment was previously shown.
     */
    private void previousButtonClicked() {
        saveGivenAnswer();
        if (!questionsStack.empty()) {
            questionViewPager.setCurrentItem(questionsStack.pop(), true);
        }
    }

    /**
     * Determine which fragment is shown next.
     */
    private void nextButtonClicked() {
        if (!currentFragmentHasValidAnswer()) {
            showInvalidAnswerMessage();
            return;
        }
        saveGivenAnswer();
        StepModel step = getStep(questions.get(getCurrentPosition(0)).getStepId());
        if (questions.get(getCurrentPosition(0)).getAnswers() != null &&
                questions.get(getCurrentPosition(0)).getAnswers().size() > 0) {
            if (questions.get(getCurrentPosition(0)).getAnswers().get(0).getType().equals("radiobutton")) {
                Fragment f = questionPagerAdapter.getCurrentFragment();
                if (f instanceof RadioQuestionFragment) {
                    if (((RadioQuestionFragment) f).getSelectedAnswer() != null) {
                        int stepId = ((RadioQuestionFragment) f).getSelectedAnswer().getStepId();
                        if (stepId != 0 && getPositionForStepId(stepId) >= 0) {
                            questionsStack.push(getCurrentPosition(0));
                            questionViewPager.setCurrentItem(getPositionForStepId(stepId), true);
                            return;
                        }
                    }
                }
            }
        }
        if (step != null) {
            if (step.getQuestions().size() == questions.get(getCurrentPosition(0)).getSequence()) {
                if (step.getNextStep() != 0) {
                    questionsStack.push(getCurrentPosition(0));
                    questionViewPager.setCurrentItem(getPositionForStepId(step.getNextStep()), true);
                    return;
                } else {
                    questionsStack.push(getCurrentPosition(0));
                    questionViewPager.setCurrentItem(questions.size(), true);
                    return;
                }
            }
        }
        questionsStack.push(getCurrentPosition(0));
        questionViewPager.setCurrentItem(getCurrentPosition(+1), true);
    }

    /**
     * Check whether or not the current fragment contains a valid answer.
     */
    private boolean currentFragmentHasValidAnswer() {
        if (questions.get(getCurrentPosition(0)).getSkippable()) {
            return true;
        }
        Fragment f = questionPagerAdapter.getCurrentFragment();
        return f instanceof QuestionFragment && ((QuestionFragment) f).hasValidAnswer();
    }

    /**
     * Show error message when the provided answer is invalid.
     */
    private void showInvalidAnswerMessage() {
        View v = findViewById(android.R.id.content);
        SnackbarHelper.showErrorSnackbar(v, R.string.prompt_diary_answer_invalid, 0, null);
    }

    /**
     * Call method in shown fragment to save given answer.
     */
    private void saveGivenAnswer() {
        Fragment f = questionPagerAdapter.getCurrentFragment();
        if (f instanceof QuestionFragment) {
            ((QuestionFragment) f).saveAnswer();
        }
    }

    /**
     * Represents the API call to insert a diary entry.
     * @param diaryEntryParameter the diary entry model
     */
    private void insertDiaryEntry(final DiaryEntryParameter diaryEntryParameter) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.entries(new DBUser(getApplicationContext()).getUser().getKey(), diaryEntryParameter);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,
                                   @NonNull Response<ResponseBody> response) {
                if (response.code() == 200) {
                    submitSuccessful(true, diaryEntryParameter);
                } else {
                    submitSuccessful(false, diaryEntryParameter);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                submitSuccessful(false, diaryEntryParameter);
            }
        });
    }

    /**
     * Called when diary entry insertion is successful or failed. Depending on which is true either
     * a the activity is finished or a failed message will be shown.
     * @param successful true if call was successful
     * @param diaryEntryParameter the inserted model
     */
    private void submitSuccessful(boolean successful, final DiaryEntryParameter diaryEntryParameter) {
        if (successful) {
            AppController.getInstance().clearGivenAnswers();
            finish();
        } else {
            View v = findViewById(android.R.id.content);
            SnackbarHelper.showErrorSnackbar(v, R.string.prompt_diary_entry_failed, R.string.retry,
                    new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    insertDiaryEntry(diaryEntryParameter);
                }
            });
        }
    }

    /**
     * Get the step using its id.
     *
     * @param stepId the id of the step
     * @return the step
     */
    private StepModel getStep(int stepId) {
        for (StepModel step:steps) {
            if (step.getId() == stepId)
                return step;
        }
        return null;
    }

    /**
     * Get the question using its id and step id.
     * @param stepId the id of the step
     * @param questionId the id of the question
     * @return the question
     */
    private QuestionModel getQuestion(int stepId, int questionId) {
        StepModel step = getStep(stepId);
        if (step != null) {
            for (QuestionModel question : step.getQuestions()) {
                if (question.getId() == questionId) {
                    return question;
                }
            }
        }
        return null;
    }

    /**
     * Get the answer using its id, step is and question id.
     * @param stepId the id of the step
     * @param questionId the id of the question
     * @param answerId the id of the answer
     * @return the answer
     */
    private AnswerModel getAnswer(int stepId, int questionId, int answerId) {
        StepModel step = getStep(stepId);
        if (step != null) {
            QuestionModel question = getQuestion(stepId, questionId);
            if (question != null) {
                for (AnswerModel answer:question.getAnswers()) {
                    if (answer.getId() == answerId) {
                        return answer;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get the position of the step within the ViewPager.
     *
     * @param stepId the is of the step
     * @return the position of the step
     */
    private int getPositionForStepId(int stepId) {
        int previousQuestionsCount = 0;
        for (int i = 0; i < steps.size(); i++) {
            if (steps.get(i).getId() == stepId) {
                return previousQuestionsCount;
            }
            previousQuestionsCount += steps.get(i).getQuestions().size();
        }
        return -1;
    }

    /**
     * Get the current position within the ViewPager.
     *
     * @param i used as an offset
     * @return the current position
     */
    private int getCurrentPosition(int i) {
        return questionViewPager.getCurrentItem() + i;
    }

    private class QuestionsPagerAdapter extends FragmentPagerAdapter {

        private Fragment mCurrentFragment;

        QuestionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return questions.size()+1;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == questions.size()) {
                return new FinalQuestionFragment();
            }

            QuestionModel questionModel = questions.get(position);
            if (questionModel.getAnswers() != null && questionModel.getAnswers().size() > 0) {
                AnswerModel answerModel = questionModel.getAnswers().get(0);
                switch (answerModel.getType()) {
                    case "textbox":
                        return new TextQuestionFragment().newInstance(questionModel.getStepId(), questionModel.getId());
                    case "radiobutton":
                        return new RadioQuestionFragment().newInstance(questionModel.getStepId(), questionModel.getId());
                    case "slider":
                        return new SliderQuestionFragment().newInstance(questionModel.getStepId(), questionModel.getId());
                    case "checkbox":
                        return new CheckboxQuestionFragment().newInstance(questionModel.getStepId(), questionModel.getId());
                    default:
                        return null;
                }
            }
            return getItem(getCurrentPosition(1));
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentFragment() != object) {
                mCurrentFragment = ((Fragment) object);
            }
            super.setPrimaryItem(container, position, object);
        }

        Fragment getCurrentFragment() {
            return mCurrentFragment;
        }
    }

    /**
     * Custom ViewPager which disables swiping between pages.
     *
     * @author Kevin
     */
    public static class QuestionViewPager extends ViewPager {

        public QuestionViewPager(Context context) {
            super(context);
        }

        public QuestionViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent event) {
            return false;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            return false;
        }
    }
}
