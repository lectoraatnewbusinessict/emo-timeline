package com.emotimeline.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emotimeline.R;
import com.emotimeline.fragments.CoachFragment;
import com.emotimeline.fragments.DiaryOverviewFragment;
import com.emotimeline.fragments.GraphFragment;
import com.emotimeline.fragments.SettingsFragment;
import com.emotimeline.fragments.TimelineFragment;
import com.emotimeline.fragments.TransferFragment;
import com.emotimeline.fragments.TransferingFragment;
import com.emotimeline.localdb.DBUser;
import com.emotimeline.models.localdb.User;
import com.emotimeline.utils.ImageConverter;

/**
 * This class is the MainActivity of the application.
 * It implements a NavigationView
 * @author Frank
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    public static Boolean firstStart = true;

    private User user;

    /**
     * Default onCreate method for Android
     * The theme of the App is set, the toolbar is created and a NavigationView is created
     * @param savedInstanceState state of the App
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        intro();
        setContentView(R.layout.activity_main);

        setToolbarWithDrawer();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        RelativeLayout header = (RelativeLayout) findViewById(R.id.header);
        header.setZ(1000);

        displayView( getIntent().getIntExtra("displayFragment", R.id.nav_timeline) );
    }

    /**
     * In the onResume, the setCoachData() is called.
     */
    @Override
    public void onResume(){
        super.onResume();
        setCoachData();
        GraphFragment gFragment = (GraphFragment) getSupportFragmentManager()
                .findFragmentByTag("GraphFragment");
        if (gFragment != null) {
            gFragment.setFragmentManager(getSupportFragmentManager());
        }
    }

    /**
     * This class handles the presses of the backbutton. If the drawer is open, it closes, else
     * the superclass backbutton is pressed
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * This class handles the selection in the navigationdrawer
     * @param item Menuitem from navigationdrawer
     * @return boolean
     */
    @Override
    public boolean onNavigationItemSelected(@Nullable MenuItem item) {
        if (item != null) {
            displayView(item.getItemId());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the displayview for the class
     * Sets fragment, title, and buttons for different views
     * @param viewId id of current view
     */
    public void displayView(int viewId) {
        Fragment fragment = getFragmentForView(viewId);
        String title = getTitleForView(viewId);
        setProfileTextAndButton(viewId);

        ((TextView)findViewById(R.id.headerText)).setText(title);

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     * Private method to get the fragment
     * @param viewId id of current view
     * @return Fragment to show
     */
    private Fragment getFragmentForView(int viewId){
        Fragment fragment = null;
        switch (viewId) {
            case R.id.nav_timeline:
                fragment = new TimelineFragment();
                break;
            case R.id.nav_graphs:
                GraphFragment gFragment = new GraphFragment();
                gFragment.setFragmentManager(getSupportFragmentManager());
                fragment = gFragment;
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;
            case R.id.device_change:
                fragment = new TransferFragment();
                break;
            case R.id.transfering_fragment_layout:
                fragment = new TransferingFragment();
                break;
            case R.id.nav_diary:
                fragment = new DiaryOverviewFragment();
                break;
        }
        return fragment;
    }

    /**
     * Private method to get the title for the view
     * @param viewId id of current view
     * @return title for View
     */
    private String getTitleForView(int viewId) {
        String title = getString(R.string.app_name);
        switch (viewId) {
            case R.id.nav_timeline:
                title  = getResources().getString(R.string.header_text_timeline);
                break;
            case R.id.nav_graphs:
                title  = getResources().getString(R.string.header_text_graphs);
                break;
            case R.id.nav_settings:
                title = getResources().getString(R.string.header_text_settings);
                break;
            case R.id.nav_diary:
                title = getResources().getString(R.string.diary_title);
        }
        return title;
    }

    /**
     * Private metho to set the Profile text and button if the view is the nav_timeline
     * @param viewId id of current view
     */
    private void setProfileTextAndButton(int viewId){
        TextView text = (TextView) findViewById(R.id.profile_text);
        Button button = (Button) findViewById(R.id.profile_button);
        if(viewId == R.id.nav_timeline){
            this.setCoachData();
            text.setVisibility(View.VISIBLE);
            button.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.INVISIBLE);
            button.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Sets the coachData for the user
     * If it is the first time the app is started, the coachData isn't set
     */
    public void setCoachData() {
        //  Create a new boolean and preference and set it to true
        setUser(new DBUser(getApplicationContext()).getUser());

        if (isFirstStart() || user == null ) {
            return;
        }

        setCoachImage(user);
        setCoachName(user);
    }

    /**
     * Sets the image for the Coach
     * @param user The User
     */
    private void setCoachImage(User user){
        Button button = (Button) findViewById(R.id.profile_button);
        if(user.getCoachImage() != null ) {
            button.setBackground(
                    new BitmapDrawable(
                            getResources(),
                            ImageConverter.getCroppedBitmap(
                                    ImageConverter.bytesToBitmap(
                                            user.getCoachImage()
                                    )
                            )
                    )
            );
        }
    }

    /**
     * Sets the name for the Coach
     * @param user The user
     */
    private void setCoachName(User user){
        if(user.getCoachName() != null ) {
            TextView text = (TextView) findViewById(R.id.profile_text);
            text.setText(user.getCoachName());
        }
    }

    /**
     * Called to hide the software keyboard
     * @param view Current view
     */
    public static void hideSoftKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext()
                                                .getSystemService(Activity.INPUT_METHOD_SERVICE );
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow( view.getWindowToken(), 0 );
        }
    }

    /**
     * Handles the click on the coach
     * @param v The view which the click is coming from
     */
    public void clickCoach(View v){
        Fragment fragment = new CoachFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, fragment.getTag()).addToBackStack(fragment.getTag()).commit();
    }

    /**
     * This checks if the app is started for the first time
     * If this is so, IntroActivity is started
     * Uses Threads
     */
    public void intro()
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
                if (isFirstStart) {
                    Intent i = new Intent(MainActivity.this, IntroActivity.class);
                    startActivity(i);
                }
            }
        });
        t.start();
    }

    /**
     * Initialize the toolbar for the app
     */
    private void setToolbarWithDrawer()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    /**
     * Checks if the application is started for the first time
     * @return true if it is the first time the app starts, false if it has started earlier
     */
    private boolean isFirstStart()
    {
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        return getPrefs.getBoolean("firstStartC", true);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
