package com.emotimeline.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Generates a temp key with SHA256 and Salt.
 *
 * @author Kevin
 */
public class Encryption {

    /**
     * Generates a temporary key for server communication.
     *
     * @param hash received by SHA256
     * @return temporary key
     * @throws UnsupportedEncodingException thrown if encoding is unsupported
     * @throws NoSuchAlgorithmException thrown if algorithm could not be found
     */
    public static String generateTempKey(String hash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String SALT = "fdslj!katoi8423-0qtf52@hnvnq3=9853-4toijE#SM%Ldfkgjqp5978";
        return sha256(hash + SALT);
    }

    /**
     * Hashes the given String using SHA256.
     * 
     * @param base the String that needs encrypting
     * @return the encrypted String
     */
    static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
